monarch_power = ADM

ahead_of_time = {
	yearly_corruption = -0.05
}

technology = {
	# Tech 0
	year = 270	
	
	may_fabricate_claims = yes
	merchants = yes
	
	native_earthwork = yes
	native_palisade = yes
	native_fortified_house = yes
	native_ceremonial_fire_pit = yes
	native_irrigation = yes
	native_storehouse = yes
	native_longhouse = yes
	native_sweat_lodge = yes
	native_great_trail = yes
	native_three_sisters_field = yes
}

technology = {
	# Tech 1
	year = 285
	expects_institution = {
		ru_harmony = 0.1
	}
	
	ru_temple = yes
}

technology = {
	# Tech 2
	year = 300
	expects_institution = {
		ru_harmony = 0.2
	}
		
	
	ru_marketplace = yes
}

technology = {
	# Tech 3
	year = 315
	expects_institution = {
		ru_harmony = 0.3
	}
		
	ru_mine = yes
	ru_farm = yes
}
 
technology = {
	# Tech 4
	year = 330
	expects_institution = {
		ru_harmony = 0.4
	}
	
	allowed_idea_groups = 1
}

technology = {
	# Tech 5
	year = 345
	expects_institution = {
		ru_harmony = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 6
	year = 360
	expects_institution = {
		ru_harmony = 0.5
	}
	
	allowed_idea_groups = 2
	
	ru_manufactory = yes
	
}

technology = {
	# Tech 7
	year = 375
	expects_institution = {
		ru_harmony = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 8
	year = 390
	expects_institution = {
		ru_harmony = 0.5
	}
		
	allowed_idea_groups = 3
}

technology = {
	# Tech 9
	year = 405
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.1
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 10
	year = 420
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.2
	}
		
	allowed_idea_groups = 4
	ru_library = yes
}

technology = {
	# Tech 11
	year = 435
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.3
	}
		
	development_efficiency = 0.1
}

technology = {
	# Tech 12
	year = 450
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.4
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 13
	year = 465
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
	}
		
	production_efficiency =	0.02
	ru_guildhall = yes
}

technology = {
	# Tech 14
	year = 480
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
	}
	
	allowed_idea_groups = 5
}

technology = {
	# Tech 15
	year = 495
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 16
	year = 510
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.1
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 17
	year = 525
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.2
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 18
	year = 540
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.3
	}
		
	allowed_idea_groups = 6
}

technology = {
	# Tech 19
	year = 555
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.4
	}
		
	governing_capacity = 200
}

technology = {
	# Tech 20
	year = 570
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 21
	year = 585
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
	}
		
	administrative_efficiency = 0.1
	ru_observatory = yes
}

technology = {
	# Tech 22
	year = 600
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.1
	}
		
	allowed_idea_groups = 7
}

technology = {
	# Tech 23
	year = 615
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.2
	}
		
	governing_capacity = 200
}

technology = {
	# Tech 24
	year = 630
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.3
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 25
	year = 645
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.4
	}
		
	development_efficiency = 0.1

	ru_secondary_capital = yes
}

technology = {
	# Tech 26
	year = 660
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
	}
		
	allowed_idea_groups = 8
}

technology = {
	# Tech 27
	year = 675
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
	}
	
	governing_capacity = 200
}

technology = {
	# Tech 28
	year = 690
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 29
	year = 705
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.1
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 30
	year = 720
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.2
	}
		
	allowed_idea_groups = 9
}

technology = {
	# Tech 31
	year = 735
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.3
	}
		
	administrative_efficiency = 0.1
}

technology = {
	# Tech 32
	year = 750
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.4
	}
		
	governing_capacity = 200
}

technology = {
	# Tech 33
	year = 765
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 34
	year = 780
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
	}
	
	allowed_idea_groups = 10
}

technology = {
	# Tech 35
	year = 795
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 36
	year = 810
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.1
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 37
	year = 825
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.2
	}
		
	governing_capacity = 200
}

technology = {
	# Tech 38
	year = 840
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.3
	}
		
	allowed_idea_groups = 11
}

technology = {
	# Tech 39
	year = 855
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.4
	}
		
	development_efficiency = 0.1
}

technology = {
	# Tech 40
	year = 870
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.5
	}
		
	production_efficiency =	0.02
}

technology = {
	# Tech 41
	year = 885
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.5
	}
		
	administrative_efficiency = 0.1
}

technology = {
	# Tech 42
	year = 900
	expects_institution = {
		ru_harmony = 0.5
		ru_scholasticism = 0.5
		ru_expansionism = 0.5
		ru_feudalism = 0.5
		ru_devotion = 0.5
		ru_resurgence_of_civilization = 0.5
	}
	
	allowed_idea_groups = 12
}