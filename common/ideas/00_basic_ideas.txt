administrative_ideas = {
	category = ADM
	
	bonus = {
		core_creation = -0.1
	}
	
	administrative_idea_1 = {
		global_tax_modifier = 0.1
	}
	
	administrative_idea_2 = {
		adm_tech_cost_modifier = -0.1
	}
	
	administrative_idea_3 = {
		inflation_reduction = 0.1
	}
	
	administrative_idea_4 = {
		advisor_cost = -0.1
	}
	
	administrative_idea_5 = {
		yearly_corruption = -0.1
	}
	
	administrative_idea_6 = {
		advisor_pool = 2
	}
	
	administrative_idea_7 = {
		administrative_efficiency = 0.05
	}
	
	ai_will_do = {
		factor = 0.5
		modifier = {
			factor = 2
			inflation = 5
		}
		modifier = {
			factor = 2
			inflation = 10
		}
	}
}

centralised_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = administrative_ideas
		NOT = { has_idea_group = decentralised_ideas }
	}
	
	bonus = {
		great_project_upgrade_cost = -0.25
	}
	
	centralised_idea_1 = {
		technology_cost = -0.1
	}
	
	centralised_idea_2 = {
		state_maintenance_modifier = -0.1
	}
	
	centralised_idea_3 = {
		inflation_action_cost = -0.1
	}
	
	centralised_idea_4 = {
		possible_policy = 1
	}
	
	centralised_idea_5 = {
		idea_cost = -0.1
	}
	
	centralised_idea_6 = {
		state_governing_cost = -0.1
	}
	
	centralised_idea_7 = {
		free_policy = 1
	}
	
	ai_will_do = {
		factor = 2
	}
}

decentralised_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = administrative_ideas
		NOT = { has_idea_group = centralised_ideas }
	}
	
	bonus = {
		development_cost = -0.1
	}
	
	decentralised_idea_1 = {
		production_efficiency = 0.1
	}
	
	decentralised_idea_2 = {
		global_unrest = -2
	}
	
	decentralised_idea_3 = {
		build_cost = -0.1
	}
	
	decentralised_idea_4 = {
		global_institution_spread = 0.1
	}
	
	decentralised_idea_5 = {
		years_of_nationalism = -5
	}
	
	decentralised_idea_6 = {
		build_time = -0.1
	}
	
	decentralised_idea_7 = {
		embracement_cost = -0.1
	}
	
	ai_will_do = {
		factor = 2
	}
}

devotion_ideas = {
	category = ADM
	
	bonus = {
		harsh_treatment_cost = -0.25
	}
	
	devotion_idea_1 = {
		religious_unity = 0.25
	}
	
	devotion_idea_2 = {
		missionaries = 1
	}
	
	devotion_idea_3 = {
		tolerance_own = 1
	}
	
	devotion_idea_4 = {
		prestige_per_development_from_conversion = 0.25
	}
	
	devotion_idea_5 = {
		prestige_decay = -0.01
	}
	
	devotion_idea_6 = {
		institution_spread_from_true_faith = 0.2
	}
	
	devotion_idea_7 = {
		global_tax_modifier = 0.1
	}
	
	ai_will_do = {
		factor = 0.5
		modifier = {
			factor = 2
			NOT = { religious_unity = 0.5 }
		}
		modifier = {
			factor = 2
			NOT = { religious_unity = 0.25 }
		}
		modifier = {
			factor = 2
			technology_group = void
		}
	}
}

religious_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = devotion_ideas
		NOT = { has_idea_group = humanist_ideas }
	}
	
	bonus = {
		missionaries = 1
	}
	
	religious_idea_1 = {
		missionaries = 1
	}
	
	religious_idea_2 = {
		missionary_maintenance_cost = -0.1
	}
	
	religious_idea_3 = {
		tolerance_own = 2
	}
	
	religious_idea_4 = {
		global_missionary_strength = 0.03
	}
	
	religious_idea_5 = {
		stability_cost_modifier = -0.1
	}
	
	religious_idea_6 = {
		warscore_cost_vs_other_religion = -0.1
	}
	
	religious_idea_7 = {
		cb_on_religious_enemies = yes
	}
	
	ai_will_do = {
		factor = 2
	}
}

humanist_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = devotion_ideas
		NOT = { has_idea_group = religious_ideas }
	}
	
	bonus = {
		same_culture_advisor_cost = -0.1
	}
	
	humanist_idea_1 = {
		reform_progress_growth = 0.1
	}
	
	humanist_idea_2 = {
		tolerance_heretic = 2
	}
	
	humanist_idea_3 = {
		num_accepted_cultures = 2
	}
	
	humanist_idea_4 = {
		years_of_nationalism = -5
	}
	
	humanist_idea_5 = {
		culture_conversion_cost = -0.1
	}
	
	humanist_idea_6 = {
		tolerance_heathen = 3
	}
	
	humanist_idea_7 = {
		promote_culture_cost = -1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			technology_group = void
		}
	}
}

colonialism_ideas = {
	category = ADM
	
	bonus = {
		range = 0.5
	}
	
	colonialism_idea_1 = {
		colonists = 1
	}
	
	colonialism_idea_2 = {
		auto_explore_adjacent_to_colony = yes
	}
	
	colonialism_idea_3 = {
		native_assimilation = 0.25
	}
	
	colonialism_idea_4 = {
		trade_efficiency = 0.1
	}
	
	colonialism_idea_5 = {
		expel_minorities_cost = -0.25
	}
	
	colonialism_idea_6 = {
		governing_capacity_modifier = 0.1
	}
	
	colonialism_idea_7 = {
		movement_speed = 0.1
	}
	
	ai_will_do = {
		factor = 0.5
		modifier = {
			factor = 1000
			is_colonial_nation = yes
		}
		modifier = {
			factor = 2
			any_owned_province = {
				has_empty_adjacent_province = yes
			}
		}
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { tag = SHU }
			NOT = { tag = GSH }
			NOT = { tag = IXA }
			NOT = { tag = IXT }
			NOT = { tag = KUM }
			NOT = { tag = CAM }
			NOT = { tag = HEL }
			NOT = { tag = BLE }
			NOT = { tag = URS }
			NOT = { technology_group = void }
		}
		modifier = {
			factor = 1000
			tag = SHU
			tag = GSH
			tag = IXA
			tag = IXT
			tag = KUM
			tag = CAM
			tag = HEL
			tag = BLE
			tag = URS
		}
	}
}

exploration_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = colonialism_ideas
		NOT = { has_idea_group = expansion_ideas }
	}
	
	bonus = {
		colonist_placement_chance = 0.05
	}
	
	exploration_idea_1 = {
		may_explore = yes
	}
	
	exploration_idea_2 = {
		colonists = 1
	}
	
	exploration_idea_3 = {
		range = 0.5
	}
	
	exploration_idea_4 = {
		global_tariffs = 0.25
	}
	
	exploration_idea_5 = {
		treasure_fleet_income = 0.25
	}
	
	exploration_idea_6 = {
		merchants = 1
	}
	
	exploration_idea_7 = {
		global_colonial_growth = 25
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			is_colonial_nation = yes
		}
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			current_age = the_ruination #TooLate
		}
		modifier = {
			factor = 0
			NOT = {
				any_owned_province = {
					has_port = yes
				}
			}
		}
		modifier = {
			factor = 0
			NOT = { tag = CAM }
			NOT = { tag = HEL }
			NOT = { tag = BLE }
			NOT = { technology_group = void }
		}
		modifier = {
			factor = 1000
			tag = CAM
			tag = HEL
			tag = BLE
		}
	}
}

expansion_ideas = {
	category = ADM
	
	trigger = {
		has_idea_group = colonialism_ideas
		NOT = { has_idea_group = exploration_ideas }
	}
	
	bonus = {
		native_uprising_chance = -0.5
	}
	
	expansion_idea_1 = {
		colonists = 1
	}
	
	expansion_idea_2 = {
		global_colonial_growth = 25
	}
	
	expansion_idea_3 = {
		native_assimilation = 0.25
	}
	
	expansion_idea_4 = {
		trade_efficiency = 0.1
	}
	
	expansion_idea_5 = {
		expel_minorities_cost = -0.25
	}
	
	expansion_idea_6 = {
		colonists = 1
	}
	
	expansion_idea_7 = {
		range = 0.5
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 1000
			is_colonial_nation = yes
		}
		modifier = {
			factor = 2
			any_owned_province = {
				has_empty_adjacent_province = yes
			}
		}
		modifier = {
			factor = 0
			NOT = { is_colonial_nation = yes }
			NOT = {
				any_owned_province = {
					has_empty_adjacent_province = yes
				}
			}
		}
		modifier = {
			factor = 0
			NOT = { tag = SHU }
			NOT = { tag = GSH }
			NOT = { tag = IXA }
			NOT = { tag = IXT }
			NOT = { tag = KUM }
			NOT = { tag = CAM }
			NOT = { tag = HEL }
			NOT = { tag = BLE }
			NOT = { tag = URS }
		}
		modifier = {
			factor = 1000
			tag = SHU
			tag = GSH
			tag = IXA
			tag = IXT
			tag = KUM
			tag = URS
		}
	}
}

diplomatic_ideas = {
	category = DIP
	
	bonus = {
		ae_impact = -0.1
	}
	
	diplomatic_idea_1 = {
		improve_relation_modifier = 0.1
	}
	
	diplomatic_idea_2 = {
		dip_tech_cost_modifier = -0.1
	}
	
	diplomatic_idea_3 = {
		diplomats = 1
	}
	
	diplomatic_idea_4 = {
		diplomatic_upkeep = 1
	}
	
	diplomatic_idea_5 = {
		rival_change_cost = -0.5
	}
	
	diplomatic_idea_6 = {
		envoy_travel_time = -0.1
	}
	
	diplomatic_idea_7 = {
		diplomatic_reputation = 1
	}
	
	ai_will_do = {
		factor = 0.5
		modifier = {
			factor = 4
			is_emperor = yes
		}
		modifier = {
			factor = 2
			vassal = 2
		}
	}
}

espionage_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = diplomatic_ideas
		NOT = { has_idea_group = subject_ideas }
	}
	
	bonus = {
		rebel_support_efficiency = 0.25
	}
	
	espionage_idea_1 = {
		spy_offence = 0.25
	}
	
	espionage_idea_2 = {
		diplomats = 1
	}
	
	espionage_idea_3 = {
		global_spy_defence = 0.25
	}
	
	espionage_idea_4 = {
		improve_relation_modifier = 0.1
	}
	
	espionage_idea_5 = {
		province_warscore_cost = -0.1
	}
	
	espionage_idea_6 = {
		envoy_travel_time = -0.1
	}
	
	espionage_idea_7 = {
		diplomatic_reputation = 1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			is_emperor = yes
		}
	}
}

subject_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = diplomatic_ideas
		NOT = { has_idea_group = espionage_ideas }
	}
	
	bonus = {
		vassal_forcelimit_bonus = 1
	}
	
	subject_idea_1 = {
		vassal_income = 0.1
	}
	
	subject_idea_2 = {
		diplomats = 1
	}
	
	subject_idea_3 = {
		reduced_liberty_desire = 0.10
	}
	
	subject_idea_4 = {
		diplomatic_upkeep = 1
	}
	
	subject_idea_5 = {
		diplomatic_annexation_cost = -0.1
	}
	
	subject_idea_6 = {
		envoy_travel_time = -0.1
	}
	
	subject_idea_7 = {
		diplomatic_reputation = 1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 2
			is_emperor = yes
		}
		modifier = {
			factor = 2
			vassal = 2
		}
	}
}

trade_ideas = {
	category = DIP
	
	bonus = {
		global_ship_trade_power = 0.1
	}
	
	trade_idea_1 = {
		trade_efficiency = 0.1
	}
	
	trade_idea_2 = {
		merchants = 1
	}
	
	trade_idea_3 = {
		trade_range_modifier = 0.25
	}
	
	trade_idea_4 = {
		trade_steering = 0.1
	}
	
	trade_idea_5 = {
		global_trade_power = 0.1
	}
	
	trade_idea_6 = {
		light_ship_cost = -0.1
	}
	
	trade_idea_7 = {
		light_ship_power = 0.1
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { num_of_cities = 5 }
		}
		modifier = {
			factor = 1000
			has_reform = pirate_republic_reform
		}
	}
}

privateer_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = trade_ideas
		NOT = { has_idea_group = caravan_ideas }
	}
	
	bonus = {
		capture_ship_chance = 0.1
	}
	
	privateer_idea_1 = {
		may_perform_slave_raid = yes
	}
	
	privateer_idea_2 = {
		privateer_efficiency = 0.1
	}
	
	privateer_idea_3 = {
		embargo_efficiency = 0.1
	}
	
	privateer_idea_4 = {
		merchants = 1
	}
	
	privateer_idea_5 = {
		trade_range_modifier = 0.25
	}
	
	privateer_idea_6 = {
		light_ship_cost = -0.1
	}
	
	privateer_idea_7 = {
		light_ship_power = 0.1
	}
	
	ai_will_do = {
		factor = 1000
		modifier = {
			factor = 0
			NOT = { has_reform = pirate_republic_reform }
		}
	}
}

caravan_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = trade_ideas
		NOT = { has_idea_group = privateer_ideas }
	}
	
	bonus = {
		trade_steering = 0.1
	}
	
	caravan_idea_1 = {
		caravan_power = 0.1
	}
	
	caravan_idea_2 = {
		merchants = 1
	}
	
	caravan_idea_3 = {
		global_trade_goods_size_modifier = 0.1
	}
	
	caravan_idea_4 = {
		trade_range_modifier = 0.25
	}
	
	caravan_idea_5 = {
		global_prov_trade_power_modifier = 0.1
	}
	
	caravan_idea_6 = {
		caravan_power = 0.1
	}
	
	caravan_idea_7 = {
		merchants = 1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			NOT = { num_of_cities = 5 }
		}
	}
}

maritime_ideas = {
	category = DIP
	
	bonus = {
		admiral_cost = -0.1
	}
	
	maritime_idea_1 = {
		global_ship_recruit_speed = -0.1
	}
	
	maritime_idea_2 = {
		prestige_from_naval = 0.25
	}
	
	maritime_idea_3 = {
		global_ship_repair = 0.1
	}
	
	maritime_idea_4 = {
		navy_tradition_decay = -0.1
	}
	
	maritime_idea_5 = {
		global_sailors_modifier = 1
	}
	
	maritime_idea_6 = {
		sailors_recovery_speed = 1
	}
	
	maritime_idea_7 = {
		naval_tradition_from_battle = 0.25
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}
		modifier = {
			factor = 0.5
			NOT = { num_of_ports = 10 }
		}
		modifier = {
			factor = 1000
			has_reform = pirate_republic_reform
		}
	}
}

navy_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = maritime_ideas
		NOT = { has_idea_group = armada_ideas }
	}
	
	bonus = {
		global_naval_barrage_cost = -0.1
	}
	
	navy_idea_1 = {
		heavy_ship_cost = -0.1
	}
	
	navy_idea_2 = {
		heavy_ship_power = 0.1
	}
	
	navy_idea_3 = {
		ship_durability = 0.1
	}
	
	navy_idea_4 = {
		naval_morale = 0.1
	}
	
	navy_idea_5 = {
		recover_navy_morale_speed = 0.1
	}
	
	navy_idea_6 = {
		transport_cost = -0.1
	}
	
	navy_idea_7 = {
		transport_power = 0.1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}
		modifier = {
			factor = 0.5
			NOT = { num_of_ports = 10 }
		}
	}
}

armada_ideas = {
	category = DIP
	
	trigger = {
		has_idea_group = maritime_ideas
		NOT = { has_idea_group = navy_ideas }
	}
	
	bonus = {
		naval_forcelimit_modifier = 0.5
	}
	
	armada_idea_1 = {
		galley_cost = -0.1
	}
	
	armada_idea_2 = {
		galley_power = 0.1
	}
	
	armada_idea_3 = {
		naval_maintenance_modifier = -0.1
	}
	
	armada_idea_4 = {
		sunk_ship_morale_hit_recieved = -0.1
	}
	
	armada_idea_5 = {
		disengagement_chance = 0.1
	}
	
	armada_idea_6 = {
		global_naval_engagement_modifier = 0.25
	}
	
	armada_idea_7 = {
		allowed_marine_fraction = 0.25
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}
		modifier = {
			factor = 0.5
			NOT = { num_of_ports = 10 }
		}
		modifier = {
			factor = 1000
			has_reform = pirate_republic_reform
		}
	}
}

military_ideas = {
	category = MIL
	
	bonus = {
		siege_blockade_progress = 1
	}
	
	military_idea_1 = {
		prestige_from_land = 0.1
	}
	
	military_idea_2 = {
		mil_tech_cost_modifier = -0.1
	}
	
	military_idea_3 = {
		army_tradition_decay = -0.1
	}
	
	military_idea_4 = {
		available_province_loot = 0.25
	}
	
	military_idea_5 = {
		land_maintenance_modifier = -0.1
	}
	
	military_idea_6 = {
		land_morale = 0.1
	}
	
	military_idea_7 = {
		loot_amount = 0.25
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1000
			technology_group = void
		}
	}
}

shock_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = military_ideas
		NOT = { has_idea_group = fire_ideas }
	}
	
	bonus = {
		army_tradition = 1
	}
	
	shock_idea_1 = {
		shock_damage = 0.1
	}
	
	shock_idea_2 = {
		shock_damage_received = -0.1
	}
	
	shock_idea_3 = {
		infantry_shock = 1
	}
	
	shock_idea_4 = {
		cavalry_shock = 1
	}
	
	shock_idea_5 = {
		artillery_shock = 1
	}
	
	shock_idea_6 = {
		leader_land_shock = 1
	}
	
	shock_idea_7 = {
		leader_naval_shock = 1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 1000
			technology_group = void
		}
	}
}

fire_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = military_ideas
		NOT = { has_idea_group = shock_ideas }
	}
	
	bonus = {
		army_tradition = 1
	}
	
	fire_idea_1 = {
		fire_damage = 0.1
	}
	
	fire_idea_2 = {
		fire_damage_received = -0.1
	}
	
	fire_idea_3 = {
		infantry_fire = 1
	}
	
	fire_idea_4 = {
		cavalry_fire = 1
	}
	
	fire_idea_5 = {
		artillery_fire = 1
	}
	
	fire_idea_6 = {
		leader_land_fire = 1
	}
	
	fire_idea_7 = {
		leader_naval_fire = 1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			technology_group = void
		}
	}
}

army_design_ideas = {
	category = MIL
	
	bonus = {
		general_cost = -0.10
	}
	
	army_design_idea_1 = {
		free_leader_pool = 1
	}
	
	army_design_idea_2 = {
		cav_to_inf_ratio = 0.25
	}
	
	army_design_idea_3 = {
		possible_condottieri = 1
	}
	
	army_design_idea_4 = {
		yearly_army_professionalism = 0.01
	}
	
	army_design_idea_5 = {
		cavalry_flanking = 0.25
	}
	
	army_design_idea_6 = {
		artillery_levels_available_vs_fort = 1
	}
	
	army_design_idea_7 = {
		army_tradition_from_battle = 0.25
	}
	
	ai_will_do = {
		factor = 1
	}
}

quantity_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = army_design_ideas
		NOT = { has_idea_group = quality_ideas }
	}
	
	bonus = {
		land_forcelimit_modifier = 0.5
	}
	
	quantity_idea_1 = {
		land_maintenance_modifier = -0.1
	}
	
	quantity_idea_2 = {
		merc_maintenance_modifier = -0.1
	}
	
	quantity_idea_3 = {
		global_regiment_cost = -0.1
	}
	
	quantity_idea_4 = {
		mercenary_cost = -0.1
	}
	
	quantity_idea_5 = {
		global_manpower_modifier = 0.25
	}
	
	quantity_idea_6 = {
		manpower_recovery_speed = 0.25
	}
	
	quantity_idea_7 = {
		mercenary_discipline = 0.05
	}
	
	ai_will_do = {
		factor = 2
	}
}

quality_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = army_design_ideas
		NOT = { has_idea_group = quantity_ideas }
	}
	
	bonus = {
		mercenary_discipline = 0.05
	}
	
	quality_idea_1 = {
		land_morale = 0.1
	}
	
	quality_idea_2 = {
		drill_gain_modifier = 0.05
	}
	
	quality_idea_3 = {
		recover_army_morale_speed = 0.1
	}
	
	quality_idea_4 = {
		drill_decay_modifier = -0.25
	}
	
	quality_idea_5 = {
		siege_ability = 0.1
	}
	
	quality_idea_6 = {
		backrow_artillery_damage = 0.1
	}
	
	quality_idea_7 = {
		discipline = 0.05
	}
	
	ai_will_do = {
		factor = 2
	}
}

leadership_ideas = {
	category = MIL
	
	bonus = {
		admiral_skill_gain_modifier = 0.25
	}
	
	leadership_idea_1 = {
		leader_land_manuever = 1
	}
	
	leadership_idea_2 = {
		leader_naval_manuever = 1
	}
	
	leadership_idea_3 = {
		free_leader_pool = 1
	}
	
	leadership_idea_4 = {
		drill_gain_modifier = 0.10
	}
	
	leadership_idea_5 = {
		movement_speed = 0.10
	}
	
	leadership_idea_6 = {
		leader_cost = -0.10
	}
	
	leadership_idea_7 = {
		leader_siege = 1
	}
	
	ai_will_do = {
		factor = 1
	}
}

offensive_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = leadership_ideas
		NOT = { has_idea_group = defensive_ideas }
	}
	
	bonus = {
		artillery_levels_available_vs_fort = 1
	}
	
	offensive_idea_1 = {
		war_exhaustion = -0.1
	}
	
	offensive_idea_2 = {
		infantry_power = 0.15
	}
	
	offensive_idea_3 = {
		reinforce_speed = 0.1
	}
	
	offensive_idea_4 = {
		cavalry_power = 0.15
	}
	
	offensive_idea_5 = {
		global_regiment_recruit_speed = -0.1
	}
	
	offensive_idea_6 = {
		artillery_power = 0.15
	}
	
	offensive_idea_7 = {
		land_attrition = -0.25
	}
	
	ai_will_do = {
		factor = 2
	}
}

defensive_ideas = {
	category = MIL
	
	trigger = {
		has_idea_group = leadership_ideas
		NOT = { has_idea_group = offensive_ideas }
	}
	
	bonus = {
		rival_border_fort_maintenance = -0.1
	}
	
	defensive_idea_1 = {
		fort_maintenance_modifier = -0.1
	}
	
	defensive_idea_2 = {
		defensiveness = 0.1
	}
	
	defensive_idea_3 = {
		hostile_attrition = 2
	}
	
	defensive_idea_4 = {
		recover_army_morale_speed = 0.1
	}
	
	defensive_idea_5 = {
		global_supply_limit_modifier = 0.1
	}
	
	defensive_idea_6 = {
		leader_cost = -0.10
	}
	
	defensive_idea_7 = {
		war_taxes_cost_modifier = -0.1
	}
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			technology_group = void
		}
	}
}