#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 85 194 60 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	military_conduct
	tech_lv1_transport_ideas
	tech_lv2_development_ideas
	tech_lv3_heavy_ship_ideas
	magic_lv3_spirit_school
	magic_lv2_ascended_school
	magic_lv1_water_school
	magic_lv1_magma_school
}

historical_units = {
	Freljordian_Artillery_1
	Freljordian_Artillery_2
	Freljordian_Artillery_3
	Freljordian_Artillery_4
	Freljordian_Artillery_5
	Freljordian_Cavalry_1
	Freljordian_Cavalry_2
	Freljordian_Cavalry_3
	Freljordian_Cavalry_4
	Freljordian_Cavalry_5
	Freljordian_Infantry_1
	Freljordian_Infantry_2
	Freljordian_Infantry_3
	Freljordian_Infantry_4
	Freljordian_Infantry_5
}

monarch_names = {
	"Hejian #0" = 100
	"Najak #0" = 100
	"Sigvar #0" = 100
	"Alfhuild #0" = 100
	"Asgerhi #0" = 100
	"Asmond #0" = 100
	"Yoan #0" = 100
	"Aalve #0" = 100
	"Aste #0" = 100
	"Gunnar #0" = 100
	"Gunvor #0" = 100
	"Galbat #0" = 100
	"Iver #0" = 100
	"Ivan #0" = 100
	"Knul #0" = 100
	"Donan #0" = 100
	"Latham #0" = 100
	"Loksam #0" = 100
	"Steen #0" = 100
	"Svatohe #0" = 100
	"Sev #0" = 100
	"Thorpe #0" = 100
	"Svatveti #0" = 100
	"Peran #0" = 100
	"Gurvand #0" = 100
	"Yngvar #0" = 100
	"Houarvian #0" = 100
	"Mautith #0" = 100
	"Tynre #0" = 100
	"Kadot #0" = 100
	"Brohac #0" = 100

	"Kalkia #0" = -100
	"Najak #0" = -100
	"Briaca #0" = -100
	"Alfhuild #0" = -100
	"Asgerhi #0" = -100
	"Eozena #0" = -100
	"Aste #0" = -100
	"Folkea #0" = -100
	"Erika #0" = -100
	"Tirid #0" = -100
	"Galbat #0" = -100
	"Helle #0" = -100
	"Glannon #0" = -100
	"Knul #0" = -100
	"Latham #0" = -100
	"Loksam #0" = -100
	"Rula #0" = -100
	"Yana #0" = -100
	"Svatohe #0" = -100
	"Rad #0" = -100
	"Svatveti #0" = -100
	"Tura #0" = -100
	"Tyroe #0" = -100
	"Magod #0" = -100
	"Tudon #0" = -100
	"Alefir #0" = -100
	"Gael #0" = -100
	"Albinnen #0" = -100
	"Marit #0" = -100
	"Innga #0" = -100
}

leader_names = {
	Hejian
	Najak
	Sigvar
	Alfhuild
	Asgerhi
	Asmond
	Yoan
	Aalve
	Aste
	Gunnar
	Gunvor
	Galbat
	Iver
	Ivan
	Knul
	Donan
	Latham
	Loksam
	Steen
	Svatohe
	Sev
	Thorpe
	Svatveti
	Peran
	Gurvand
	Yngvar
	Houarvian
	Mautith
	Tynre
	Kadot
	Brohac
}

ship_names = {
	Tanzaneite Holtowite Lanrkite Salatierite Smokicoatite Chone Peroile Vermilion Auburn 
	"The Grand Tempest" "The Scented Valor" "The Parallel Vow" "The Curly Dewdrop"
}

army_names = {
	"Army of $PROVINCE$" 
}

fleet_names = {
	"Fleet of $PROVINCE$"
}