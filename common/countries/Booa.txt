#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 65 0 255 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	administrative_conduct
	tech_lv1_infantry_ideas
	magic_lv1_magma_school
	tech_lv2_cavalry_ideas
	tech_lv3_artillery_ideas
	magic_lv3_spirit_school
	magic_lv3_celestial_school
	magic_lv3_blood_school
}

historical_units = {
	Ionian_Artillery_1
	Ionian_Artillery_2
	Ionian_Artillery_3
	Ionian_Artillery_4
	Ionian_Artillery_5
	Ionian_Cavalry_1
	Ionian_Cavalry_2
	Ionian_Cavalry_3
	Ionian_Cavalry_4
	Ionian_Cavalry_5
	Ionian_Infantry_1
	Ionian_Infantry_2
	Ionian_Infantry_3
	Ionian_Infantry_4
	Ionian_Infantry_5
}

monarch_names = {
	"Wakai #0" = 100
	"Kisho #0" = 100
	"Kibe #0" = 100
	"Tetsu #0" = 100
	"Tsuji #0" = 100
	"Motoki #0" = 100
	"Hata #0" = 100
	"Noringa #0" = 100
	"Gomi #0" = 100
	"Kanko #0" = 100
	"Matsumura #0" = 100
	"Atshushi #0" = 100
	"Haruta #0" = 100
	"Nobuyoki #0" = 100
	"Tamatsuki #0" = 100
	"Kagehissa #0" = 100
	"Shinsato #0" = 100
	"Daisetsu #0" = 100
	"Uchikoshi #0" = 100
	"Toshiaki #0" = 100
	"Tashiro #0" = 100
	"Kunio #0" = 100
	"Fujimoto #0" = 100
	"Kazu #0" = 100
	"Oishi #0" = 100
	"Otojiro #0" = 100
	"Togoshi #0" = 100
	"Takehide #0" = 100
	"Wakayoshi #0" = 100
	"Hokichi #0" = 100
	
	"Wakino #0" = -100
	"Emi #0" = -100
	"Watanuki #0" = -100
	"Kazue #0" = -100
	"Orio #0" = -100
	"Den #0" = -100
	"Matsuzaki #0" = -100
	"Osami #0" = -100
	"Yamasato #0" = -100
	"Yumiko #0" = -100
	"Sakai #0" = -100
	"Kyouko #0" = -100
	"Hora #0" = -100
	"Emao #0" = -100
	"Murakami #0" = -100
	"Kaminari #0" = -100
	"Otani #0" = -100
	"Yoshe #0" = -100
	"Yukimori #0" = -100
	"Sayomi #0" = -100
	"Eto #0" = -100
	"Akeno #0" = -100
	"Yoshimura #0" = -100
	"Fujiko #0" = -100
	"Hiruma #0" = -100
	"Yei #0" = -100
	"Uchibayashi #0" = -100
	"Nao #0" = -100
	"Yoichi #0" = -100
	"Chifumi #0" = -100
}

leader_names = {
	Wakai
	Kisho
	Kibe
	Tetsu
	Tsuji
	Motoki
	Hata
	Noringa
	Gomi
	Kanko
	Matsumura
	Atshushi
	Haruta
	Nobuyoki
	Tamatsuki
	Kagehissa
	Shinsato
	Daisetsu
	Uchikoshi
	Toshiaki
	Tashiro
	Kunio
	Fujimoto
	Kazu
	Oishi
	Otojiro
	Togoshi
	Takehide
	Wakayoshi
	Hokichi
}

ship_names = {
	Tanzaneite Holtowite Lanrkite Salatierite Smokicoatite Chone Peroile Vermilion Auburn 
	"The Grand Tempest" "The Scented Valor" "The Parallel Vow" "The Curly Dewdrop"
}

army_names = {
	"Armee von $PROVINCE$" 
}

fleet_names = {
	"Stadtflotte"
}
