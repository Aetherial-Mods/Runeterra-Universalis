#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 183 90 179 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	administrative_conduct
	tech_lv1_infantry_ideas
	magic_lv1_magma_school
	tech_lv2_cavalry_ideas
	tech_lv3_artillery_ideas
	magic_lv3_spirit_school
	magic_lv3_celestial_school
	magic_lv3_blood_school
}

historical_units = {
	Ixtaly_Artillery_1
	Ixtaly_Artillery_2
	Ixtaly_Artillery_3
	Ixtaly_Artillery_4
	Ixtaly_Artillery_5
	Ixtaly_Cavalry_1
	Ixtaly_Cavalry_2
	Ixtaly_Cavalry_3
	Ixtaly_Cavalry_4
	Ixtaly_Cavalry_5
	Ixtaly_Infantry_1
	Ixtaly_Infantry_2
	Ixtaly_Infantry_3
	Ixtaly_Infantry_4
	Ixtaly_Infantry_5
}

monarch_names = {
	"Matlalihuitl #0" = 100
	"Tochtli #0" = 100
	"Huitzilihuitl #0" = 100
	"Miztli #0" = 100
	"Xochipepe #0" = 100
	"Chicacotl #0" = 100
	"Tonalnya #0" = 100
	"Yolton #0" = 100
	"Huitzilnoch #0" = 100
	"Xitia #0" = 100
	"Huitzilin #0" = 100
	"Tlaloc #0" = 100
	"Maxtla #0" = 100
	"Ichtaca #0" = 100
	"Eleuia #0" = 100
	"Ezcotl #0" = 100
	"Ihuicazoh #0" = 100
	"Itzcoh #0" = 100
	"Xihuihua #0" = 100
	"Citlaticue #0" = 100
	"Huitztecol #0" = 100
	"Eloxochitl #0" = 100
	"Zolin #0" = 100
	"Quetzalcoatl #0" = 100
	"Milintica #0" = 100
	"Nenenolli #0" = 100
	"Ilhicapilli #0" = 100
	"Xicohtencatl #0" = 100
	"Yaoquetzal #0" = 100
	"Zelcapan #0" = 100

	"Itztli #0" = -100
	"Icnoyotl #0" = -100
	"Etalpalli #0" = -100
	"Chicomecoatl #0" = -100
	"Chalchiuhticue #0" = -100
	"Yoliyamanipilli #0" = -100
	"Mecapin #0" = -100
	"Tlazohuitl #0" = -100
	"Nochehuatl #0" = -100
	"Mizhua #0" = -100
	"Xochipilli #0" = -100
	"Yaretzi #0" = -100
	"Xocoh #0" = -100
	"Zyanya #0" = -100
	"Teyacapan #0" = -100
	"Yolozuma #0" = -100
	"Cipactli #0" = -100
	"Ohtic #0" = -100
	"Quauhcayotl #0" = -100
	"Totomapichtli #0" = -100
	"Xiuhtonal #0" = -100
	"Citlalmina #0" = -100
	"Cuicatl #0" = -100
	"Nenetl #0" = -100
	"Xipil #0" = -100
	"Eloxozoc #0" = -100
	"Cualtzin #0" = -100
	"Citlaltencatl #0" = -100
	"Acahua #0" = -100
	"Montelaloni #0" = -100
}

leader_names = {
	Matlalihuitl
	Tochtli
	Huitzilihuitl
	Miztli
	Xochipepe
	Chicacotl
	Tonalnya
	Yolton
	Huitzilnoch
	Xitia
	Huitzilin
	Tlaloc
	Maxtla
	Ichtaca
	Eleuia
	Ezcotl
	Ihuicazoh
	Itzcoh
	Xihuihua
	Citlaticue
	Huitztecol
	Eloxochitl
	Zolin
	Quetzalcoatl
	Milintica
	Nenenolli
	Ilhicapilli
	Xicohtencatl
	Yaoquetzal
	Zelcapan
}

ship_names = {
	Xilonen Itztli Cuicatl Tepin Mazatl Xicohzoh Mahuihui Tlacaelal Iuitecuhtli Tezcatemoc
}

army_names = {
	"Armee von $PROVINCE$"
}

fleet_names = {
	"Fleet of $PROVINCE$"
}