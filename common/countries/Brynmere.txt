#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 34 107 155 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	
}

historical_units = {
	Shuriman_Infantry_1
	Shuriman_Infantry_2
	Shuriman_Infantry_3
	Shuriman_Infantry_4
	Shuriman_Infantry_5
	Shuriman_Cavalry_1
	Shuriman_Cavalry_2
	Shuriman_Cavalry_3
	Shuriman_Cavalry_4
	Shuriman_Cavalry_5
	Shuriman_Artillery_1
	Shuriman_Artillery_2
	Shuriman_Artillery_3
	Shuriman_Artillery_4
	Shuriman_Artillery_5
}

monarch_names = {
	"Khaldun #0" = 100
	"Malouf #0" = 100
	"Rhoksha #0" = 100
	"Baladir #0" = 100
	"Palet #0" = 100
	"Falouk #0" = 100
	"Lysimachus #0" = 100
	"Menkhe #0" = 100
	"Ytuli #0" = 100
	"Ylludath #0" = 100
	"Zakhiro #0" = 100
	"Horok #0" = 100
	"Madpara #0" = 100
	"Nerekitan #0" = 100
	"Satur #0" = 100
	"Majarzteh #0" = 100
	"Mejenes #0" = 100
	"Yakub #0" = 100
	"Mekatsur #0" = 100
	"Poniuves #0" = 100
	"Resephi #0" = 100
	"Mones #0" = 100
	"Antiurt #0" = 100
	"Uatot #0" = 100
	"Memphis #0" = 100
	"Karmike #0" = 100
	"Psischod #0" = 100
	"Tombut #0" = 100
	
	"Shamara #0" = -100
	"Khaldun #0" = -100
	"Zyama #0" = -100
	"Rhoksha #0" = -100
	"Setaka #0" = -100
	"Menkhe #0" = -100
	"Ytuli #0" = -100
	"Madpara #0" = -100
	"Nerekitan #0" = -100
	"Majarzteh #0" = -100
	"Mekatsur #0" = -100
	"Sati #0" = -100
	"Sajit #0" = -100
	"Resephi #0" = -100
	"Rempi #0" = -100
	"Uatot #0" = -100
	"Memphis #0" = -100
	"Artavez #0" = -100
	"Karmike #0" = -100
	"Khoonsu #0" = -100
	"Osirat #0" = -100
	"Aahn #0" = -100
	"Apune #0" = -100
	"Tekhaete #0" = -100
	"Klaetera #0" = -100
	"Minea #0" = -100
	"Saragaz #0" = -100
	"Telitva #0" = -100
	"Jahilite #0" = -100
	"Daimu #0" = -100
}

leader_names = {
	Khaldun
	Malouf
	Rhoksha
	Baladir
	Palet
	Falouk
	Lysimachus
	Menkhe
	Ytuli
	Ylludath
	Zakhiro
	Horok
	Madpara
	Nerekitan
	Satur
	Majarzteh
	Mejenes
	Yakub
	Mekatsur
	Poniuves
	Resephi
	Mones
	Antiurt
	Uatot
	Memphis
	Karmike
	Psischod
	Tombut
}

ship_names = {
	Tanzaneite Holtowite Lanrkite Salatierite Smokicoatite Chone Peroile Vermilion Auburn 
	"The Grand Tempest" "The Scented Valor" "The Parallel Vow" "The Curly Dewdrop"
}

army_names = {
	"Army of $PROVINCE$" 
}

fleet_names = {
	"Fleet of $PROVINCE$"
}