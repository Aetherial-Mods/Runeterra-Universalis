#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 140 0 180 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	administrative_conduct
	tech_lv1_infantry_ideas
	magic_lv1_magma_school
	tech_lv2_cavalry_ideas
	tech_lv3_artillery_ideas
	magic_lv3_spirit_school
	magic_lv3_celestial_school
	magic_lv3_blood_school
}

historical_units = {
	Void_Cavalry_1
	Void_Cavalry_2
	Void_Cavalry_3
	Void_Cavalry_4
	Void_Cavalry_5
}

monarch_names = {
	"Tev'Vez" = 100
	"Sar'Taki" = 100
	"Moj'Bal" = 100
	"Tru'Dku" = 100
	"Sha'Tyx" = 100
	"Mhe'Tepr" = 100
	"Kar'Mik" = 100
	"Jal'Surk" = 100
	"Kok'Gul" = 100
	"Jak'Ulx" = 100
	"Mut'Nale" = 100
	"Uti'Tal" = 100
	"Ryt'Koz" = 100
	"Cho'Torp" = 100
	"Sat'Hur" = 100
	
	"Tev'Vez" = -100
	"Sar'Taki" = -100
	"Moj'Bal" = -100
	"Tru'Dku" = -100
	"Sha'Tyx" = -100
	"Mhe'Tepr" = -100
	"Kar'Mik" = -100
	"Jal'Surk" = -100
	"Kok'Gul" = -100
	"Jak'Ulx" = -100
	"Mut'Nale" = -100
	"Uti'Tal" = -100
	"Ryt'Koz" = -100
	"Cho'Torp" = -100
	"Sat'Hur" = -100
}

leader_names = {
	Tev'Vez
	Sar'Taki
	Moj'Bal
	Tru'Dku
	Sha'Tyx
	Mhe'Tepr
	Kar'Mik
	Jal'Surk
	Kok'Gul
	Jak'Ulx
	Mut'Nale
	Uti'Tal
	Ryt'Koz
	Cho'Torp
	Sat'Hur	
}

ship_names = {
	"The Void"
}

army_names = {
	"Army of $PROVINCE$" 
}

fleet_names = {
	"Fleet of $PROVINCE$"
}