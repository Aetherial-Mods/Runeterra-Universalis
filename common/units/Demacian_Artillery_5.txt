#

unit_type = demacian
type = artillery

maneuver = 2
offensive_morale = 4
defensive_morale = 2
offensive_fire = 6
defensive_fire = 4
offensive_shock = 4
defensive_shock = 2