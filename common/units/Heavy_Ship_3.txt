type = heavy_ship

hull_size = 50
base_cannons = 80
blockade = 5

sail_speed = 10

sailors = 600

sprite_level = 3