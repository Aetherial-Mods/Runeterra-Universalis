# Theocracy
# local_defensiveness = 0.25
# local_friendly_movement_speed = 0.10
# ship_recruit_speed = -0.50
# local_monthly_devastation = -0.1
# local_development_cost = -0.15
# local_culture_conversion_cost = -0.25
# local_missionary_maintenance_cost = -0.50
# statewide_governing_cost = -0.15
# institution_growth = 1

es_provide_administrative_assistance = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { statewide_governing_cost = -0.15 }
	color = { 45 183 118 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { development = 10 }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				development = 21
			}
		}
	}
}

es_organize_enlightment_programs = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { institution_growth = 1.25 }
	color = { 240 142 66 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			current_institution = 100
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				current_institution = 95
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				NOT = { current_institution = 95 }
			}
		}
	}
}

es_spread_the_true_faith = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_missionary_maintenance_cost = -0.50 }
	color = { 25 75 34 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			has_owner_religion = yes
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				religion = ROOT
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				NOT = { religion = ROOT }
			}
		}
	}
}

es_protect_cultural_heritage = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_culture_conversion_cost = -0.25 }
	color = { 116 143 149 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			has_owner_culture = yes
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				culture = ROOT
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				NOT = { culture = ROOT }
			}
		}
	}
}

es_develop_infrastructure = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_development_cost = -0.15 }
	color = { 106 236 141 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				OR = {
					base_tax = 5
					base_production = 5
					base_manpower = 5
				}
			}
		}
		modifier = {
			factor = 5
			NOT = {
				all_province_in_state = {
					base_tax = 3
					base_production = 3
					base_manpower = 3
				}
			}
		}
	}
}

es_support_local_farmers = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_monthly_devastation = -0.1 }
	color = { 197 244 4 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			NOT = { devastation = 1 }
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { devastation = 1 }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				devastation = 10
			}
		}
	}
}

es_expand_shipyards = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { ship_recruit_speed = -0.50 }
	color = { 42 14 151 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			NOT = { has_port = yes }
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				navy_size_percentage = 0.95
			}
		}
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { has_port = yes }
			}
		}
		modifier = {
			factor = 5
			owner = {
				NOT = { navy_size_percentage = 0.50 }
			}
		}
	}
}

es_gather_additional_supplies = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_friendly_movement_speed = 0.10 }
	color = { 219 89 4 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				NOT = { is_at_war = yes }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				any_neighbor_province = {
					NOT = { owned_by = ROOT }
				}
			}
		}
	}
}

es_bless_fortress_walls = {
	potential = { government = theocracy }
	allow = { always = yes }
	modifier = { local_defensiveness = 0.25 }
	color = { 140 10 19 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				NOT = { is_at_war = yes }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				has_siege = yes
			}
		}
	}
}