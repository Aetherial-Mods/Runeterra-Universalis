ascended_group = {
	defender_of_faith = yes
	can_form_personal_unions = yes
	center_of_religion = 1333
	flag_emblem_index_range = { 110 110 }
	
	solari_lunari = {
		uses_piety = yes
		icon = 32
		color = { 135 86 168 }
		misguided_heretic = yes
		
		province = {}
		country = {
			monthly_piety = 0.001
			stability_cost_modifier = 0.05
		}
		country_as_secondary = {}
		
		on_convert = {
			change_religion = solari_lunari
			add_prestige = -100
			add_stability = -2
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = {}
	}
	solari_religion = {
		icon = 35
		color = { 255 130 0 }
		misguided_heretic = yes
		
		province = {}
		country = {
			fire_damage_received = -0.15
			stability_cost_modifier = 0.05
		}
		country_as_secondary = {}
		
		on_convert = {
			change_religion = solari_religion
		}
		
		heretic = {}
	}
	lunari_religion = {
		icon = 36
		color = { 100 142 255 }
		misguided_heretic = yes
		
		province = {}
		country = {
			fire_damage = 0.15
			stability_cost_modifier = 0.05
		}
		country_as_secondary = {}
		
		on_convert = {
			change_religion = lunari_religion
		}
		
		heretic = {}
	}
	eternals = {
		has_patriarchs = yes
		icon = 33
		color = {200 210 220}
		
		province = {
		}
		country = {
			trade_efficiency = 0.1
			development_cost = 0.1
		}
		country_as_secondary = {
		}
		
		orthodox_icons = {
			the_serpent = {
				spy_offence = 0.05
				
				allow = {
					always = yes
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_charger = {
				land_morale = 0.05
				
				allow = {
					always = yes
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_messenger = {
				spy_offence = 0.05
				land_morale = 0.05
				
				allow = {
					patriarch_authority = 0.2
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_trickster = {
				spy_offence = 0.05
				cavalry_flanking = 0.05
				
				allow = {
					patriarch_authority = 0.3
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_traveler = {
				spy_offence = 0.05
				envoy_travel_time = -0.05
				
				allow = {
					patriarch_authority = 0.4
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_warrior = {
				land_morale = 0.05
				discipline = 0.025
				
				allow = {
					patriarch_authority = 0.5
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_sisters = {
				discipline = 0.025
				diplomatic_reputation = 1
				
				allow = {
					patriarch_authority = 0.6
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_destroyer = {
				land_morale = 0.05
				siege_ability = 0.05
				
				allow = {
					patriarch_authority = 0.7
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_immortal_fire = {
				land_morale = 0.1
				siege_ability = 0.1
				
				allow = {
					patriarch_authority = 0.8
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_great_beyond = {
				cavalry_flanking = 0.15
				siege_ability = 0.15
				
				allow = {
					patriarch_authority = 0.9
				}
				ai_will_do = {
					factor = 1
				}
			}
			the_scourge = {
				land_morale = 0.2
				siege_ability = 0.2
				
				allow = {
					patriarch_authority = 1
				}
				ai_will_do = {
					factor = 1
				}
			}
		}
		
		heretic = {}
	}
	the_winged_protector = {
		icon = 42
		color = { 255 120 0 }
		misguided_heretic = yes
		
		province = {}
		country = {
			land_morale = 0.10
			tolerance_heretic = -1
		}
		country_as_secondary = {}
		
		heretic = {}
	}
	the_veiled_lady = {
		icon = 43
		color = { 120 0 255 }
		misguided_heretic = yes
		hre_religion = yes
		
		province = {}
		country = {
			defensiveness = 0.25
			diplomatic_reputation = -1
		}
		country_as_secondary = {}
		
		heretic = {}
	}
}

northern_group = {
	defender_of_faith = yes
	center_of_religion = 140
	flag_emblem_index_range = { 110 110 }
	
	freljordian_pantheon = {
		allow_female_defenders_of_the_faith = yes
		personal_deity = yes
		icon = 30
		color = { 23 204 210 }
		
		province = {
		}
		country = {
			production_efficiency = -0.05
			improve_relation_modifier = 0.25
		}
		country_as_secondary = {
		}
		
		on_convert = {
			change_religion = freljordian_pantheon
			add_prestige = -100
			add_stability = -2
			add_country_modifier = {
					name = "conversion_zeal"
					duration = 3650
			}
		}
		
		heretic = {
		}
	}
	frostguards = {
		allow_female_defenders_of_the_faith = yes
		uses_anglican_power = yes
		icon = 31
		color = { 36 63 160 }
		
		province = {
		}
		country = {
			tolerance_own = 2
			tolerance_heretic = -2
		}
		country_as_secondary = {
		}
		
		aspects = {
			government_aspect
			monopoly_aspect
			stability_aspect
		}
		
		on_convert = {
			change_religion = frostguards
			add_prestige = -100
			add_stability = -2
			add_country_modifier = {
					name = "conversion_zeal"
					duration = 3650
			}
		}
		
		heretic = {
		}
	}
}

mageborn_group = {
	flag_emblem_index_range = { 110 110 }
	religious_schools = {
        nature_school = {
            potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
				OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = nature_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = nature_school_modifier
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = nature_school_modifier
			picture = "GFX_icon_nature_school"
          	build_cost = -0.10
        }
        magma_school = {
        	potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = magma_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = magma_school_modifier 
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = magma_school_modifier
			picture = "GFX_icon_magma_school"
          	fire_damage = 0.10
        }
        water_school = {
        	potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = water_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = water_school_modifier 
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = water_school_modifier
			picture = "GFX_icon_water_school"
          	fire_damage_received = -0.10 
        }
        metal_school = {
        	potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = metal_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = metal_school_modifier 
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = metal_school_modifier
          	global_regiment_cost = -0.05
        }
        shadow_school = {
        	potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = shadow_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = shadow_school_modifier 
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = shadow_school_modifier
          	accept_vassalization_reasons = 10
        }
        light_school = {
        	potential_invite_scholar = { 
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	knows_of_scholar_country_capital_trigger = yes
            }
            can_invite_scholar = { 
            	adm_power_cost = 50
            	OR = {
					religion = axiomata
					religion = spirit_blossom
				}
            	reverse_has_opinion = { 
            		who = FROM 
            		value = 150
            	}
            	NOT = { has_country_modifier = light_school_modifier }
            	OR = { 
            		alliance_with = FROM 
            		is_subject_of = FROM
            		overlord_of = FROM
            	}
            	hidden_progressive_opinion_for_scholar_trigger = yes
            }
            on_invite_scholar = { 
            	adm_power_cost = 50 
            	clear_religious_scholar_modifiers_effect = yes 
            	custom_tooltip = INVITE_SCHOLAR_COUNTRY_TEXT 
            	add_country_modifier = { 
 					name = light_school_modifier 
 					duration = 7300 
 				}
            }
            invite_scholar_modifier_display = light_school_modifier
			picture = "GFX_icon_light_school"
          	diplomatic_reputation = 1
        }
	}
	axiomata = {
		defender_of_faith = yes
		fervor = yes
		icon = 38
		color = { 0 153 0 }
		
		province = {}
		country = {
			shock_damage = 0.10
			global_autonomy = 0.10
		}
		country_as_secondary = {
			shock_damage = 0.05
			global_autonomy = 0.05
		}
		
		on_convert = {
			change_religion = axiomata
			add_prestige = -100
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = {}
	}
	spirit_blossom = {
		personal_deity = yes
		icon = 41
		color = { 230 24 211 }
		
		province = {}
		country = {
			land_morale = -0.05
			culture_conversion_cost = -0.15
		}
		country_as_secondary = {
			land_morale = -0.05
			promote_culture_cost = -0.05
		}
		
		on_convert = {
			change_religion = spirit_blossom
			add_prestige = -100
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = {}
	}
}

nature_group = {
	flag_emblem_index_range = { 110 110 }

	mother_serpent = {
		icon = 37
		color = { 80 234 121 }
		
		province = {}
		country = {
			global_ship_cost = -0.1
			global_unrest = 1
		}
		country_as_secondary = {}
		
		on_convert = {
			change_religion = mother_serpent
			add_prestige = -100
			add_country_modifier = {
					name = "conversion_zeal"
					duration = 3650
			}
		}
		
		heretic = {}
	}
	lamb_and_wolf = {
		fetishist_cult = yes
		icon = 45
		color = { 100 100 150 }
		
		province = {}
		country = {}
		country_as_secondary = {}
		
		heretic = {}
	}
}

cults_group = {
	flag_emblem_index_range = { 110 110 }
	
	cult_of_the_spider = {
		icon = 44
		color = { 100 100 100 }
		misguided_heretic = yes
		uses_church_power = yes
		
		province = {}
		country = {
			spy_offence = 0.5
			global_unrest = 1
		}
		country_as_secondary = {}
		
		aspects = {
			spin_webs
			engage_spiderlings
			silk_road_devotion
			neurotoxin
			lay_eggs
			arachnid_administration
		}	
		
		on_convert = {
		}
		
		heretic = {}
	}
	cult_of_the_black_rose = {
		can_have_secondary_religion = yes
		icon = 5
		color = { 255 120 120 }
		declare_war_in_regency = yes
		
		province = {}
		country = {}
		country_as_secondary = {}
		
		heretic = {}
	}
	the_glorious_evolved = {
		ancestors = yes
		icon = 3
		color = { 50 128 17 }
		
		province = {
		}
		country = {
			tolerance_own = 2
			production_efficiency = 0.1
		}
		country_as_secondary = {
			merchants = 1
			tolerance_own = 1
		}
		
		on_convert = {
			change_religion = the_glorious_evolved
			add_prestige = -50
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = {
		}
	}
}

the_isles_group = {
	center_of_religion = 1618
	flag_emblem_index_range = { 110 110 }
	
	the_white_mist = {
		religious_reforms = yes
		authority = yes
		icon = 40
		color = { 120 175 150 }

		province = {
			local_development_cost = -0.1
		}
		country = {
			global_institution_spread = 0.15
			improve_relation_modifier = -0.15
		}
		country_as_secondary = {
		}

		heretic = {}
	}
	the_black_mist = {
		uses_harmony = yes
		icon = 5
		color = { 37 100 45 }

		province = {}
		country = {}
		country_as_secondary = {}
		heretic = {}
	}
	the_hallowed_mist = {
		uses_karma = yes
		icon = 39
		color = { 120 130 255 }
		misguided_heretic = yes
		
		province = {
			local_institution_spread = -0.10
		}
		country = {
			shock_damage_received = -0.10
			monthly_karma = -0.1
		}
		country_as_secondary = {
		}
		heretic = {}
	}
	great_plains = {
		icon = 5
		color = { 130 79 27 }
		
		province = {}
		country = {}
		country_as_secondary = {}
		
		on_convert = {
			change_religion = great_plains
			add_prestige = -100
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		holy_sites = { 1474 1480 1525 1831 1904 }
		
		blessings = {
			legitimize_government
			encourage_warriors_of_the_faith
			send_monks_to_establish_monasteries
			promote_territorial_rights
			will_of_the_martyrs
		}
		
		heretic = {}
	}
}

void_group = {
	center_of_religion = 1438
	flag_emblem_index_range = { 110 110 }
	defender_of_faith = yes
	
	the_watchers = {
		icon = 34
		color = { 30 30 30 }
		
		province = {
			local_missionary_strength = -0.02
		}
		country = {
			cav_to_inf_ratio = 1
			land_forcelimit_modifier = -0.25
		}
		country_as_secondary = {
		}
		
		heretic = {}
		
		papacy = {
			papal_tag = GYZ
			election_cost = 5
			seat_of_papacy = 637
			
			#Council of Trent Positions
			
			harsh = {
				relation_with_heretics = -10
				global_religious_conversion_resistance = 0.25
				global_institution_spread = -0.25
				global_missionary_strength = 0.02
			}
			neutral = {
				global_religious_conversion_resistance = -0.33
			}
			concilatory = {
				relation_with_heretics = 5
				global_religious_conversion_resistance = 0.25
				global_heretic_missionary_strength = -0.05
				improve_relation_modifier = 0.25
			}
			
			#Council of Trent Concessions
			
			concessions = {
				first_concession = {
					harsh = {
						relation_with_heretics = -10
						global_heretic_missionary_strength  = 0.02
					}	
					concilatory = {
						relation_with_heretics = 5
						tolerance_heretic = 2
					}
				}
				second_concession = {
					harsh = {
						relation_with_heretics = -10
						institution_spread_from_true_faith = 0.30
					}	
					concilatory = {
						relation_with_heretics = 5
						global_institution_spread = 0.10
					}
				}
				third_concession = {
					harsh = {
						relation_with_heretics = -10
						manpower_in_true_faith_provinces  = 0.1
					}	
					concilatory = {
						relation_with_heretics = 5
						global_manpower_modifier = 0.05
					}
				}
				fourth_concession = {
					harsh = {
						relation_with_heretics = -10
						warscore_cost_vs_other_religion  = -0.10
					}	
					concilatory = {
						relation_with_heretics = 5
						curia_powers_cost = -0.10
					}
				}
			}

			# Papal Actions below!
			
			void_sanctions = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = void_sanctions }
				}
				effect = {
					add_country_modifier = {
						name = "void_sanctions"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}				
			}
			void_blessing = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = void_blessing }
				}
				effect = {
					add_country_modifier = {
						name = "void_blessing"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}
			void_indulgence = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = void_indulgence }
				}
				effect = {
					add_country_modifier = {
						name = "void_indulgence"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}	
			void_saint = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { stability = 3 }
				}
				effect = {
					add_stability = 1
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 2
						NOT = { stability = 0 }
					}
				}
			}
			void_bargain = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = void_bargain }
				}
				effect = {
					add_country_modifier = {
						name = "void_bargain"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						NOT = { num_of_loans = 1 }
					}
				}
			}
			proclaim_void_war = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					is_at_war = yes
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = proclaim_void_war }
				}
				effect = {
					add_country_modifier = {
						name = "proclaim_void_war"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						manpower_percentage = 0.5
					}
				}
			}
			void_legate = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { has_country_modifier = void_legate }
				}
				effect = {
					add_country_modifier = {
						name = "void_legate"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}
			void_monopoly = {
				cost = 50
				potential = {
					NOT = { tag = GYZ }
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { mercantilism = 100 }
				}
				effect = {
					add_mercantilism = 1
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						NOT = { has_idea_group = trade_ideas }
					}
				}
			}
			strengthen_void = {
				curia_treasury_cost = 400
				potential = {
					has_dlc = "Emperor"
					is_papal_controller = no
				}
				allow = {
					NOT = { war_with = GYZ }
					NOT = { reform_desire = 2 }
					NOT = { has_country_modifier = strengthen_void }
				}
				effect = {
					add_reform_desire = 0.05
					add_country_modifier = {
						name = "strengthen_void"
						duration = 3650
					}
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						NOT = {
							curia_treasury_size = 1000
						}
					}
				}
			}
		}
	}
}