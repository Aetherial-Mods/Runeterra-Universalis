lillia = {
	movement_speed = 0.1
	improve_relation_modifier = 0.1
	sprite = 4
    potential = {
		religion = spirit_blossom
	}

	ai_will_do = {
		factor = 1
	}
}