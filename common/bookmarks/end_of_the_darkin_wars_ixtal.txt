bookmark = {
	name = "END_OF_THE_DARKIN_WARS_IXTAL"
	desc = "END_OF_THE_DARKIN_WARS_IXTAL_DESC"
	date = 300.1.1
	
	country = IXA
	country = HEL
	country = CAM
	country = CAL
	country = AZC
	country = YOO
	country = MOX
	
	easy_country = IXA
	
	center = 109
}