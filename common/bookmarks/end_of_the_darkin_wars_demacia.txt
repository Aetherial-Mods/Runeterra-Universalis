bookmark = {
	name = "END_OF_THE_DARKIN_WARS_DEMACIA"
	desc = "END_OF_THE_DARKIN_WARS_DEMACIA_DESC"
	date = 300.1.1
	
	country = ZEF
	country = VAS
	country = TER
	country = CLO
	country = A54
	country = A25
	country = A38
	
	easy_country = ZEF
	
	center = 357
	
	default = yes
}