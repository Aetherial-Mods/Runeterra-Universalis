
province = {
    peace = {
        active = {
            is_at_war = no
			OR = {
				NOT = { num_of_rebel_armies = 1 }
				NOT = { num_of_rebel_controlled_provinces = 1 }
			}
        }
        eval_add = {
            factor = 1000.0
			modifier = {
				factor = 7.5
				OR = {
					has_terrain = mountain
					has_terrain = highlands
					has_terrain = hills
					has_terrain = barrow
					has_terrain = dwemer_stronghold
					has_terrain = mountain_meadows
					has_terrain = orcish_stronghold
				}
			}
        }
    }
}