west_ice_sea_coast_tradenode={
	location=2097
	color={
		131 65 0
	}
	outgoing={
		name="east_ice_sea_coast_tradenode"
		path={
			2022 1755 2032 2088 1665 1431
		}
	}
	outgoing={
		name="west_freljord_tradenode"
		path={
			2112 1992 1844 217
		}
	}
	members={
		9 16 36 55 90
		105 117 137 148 154
		203 216 264 271 295
		313 341 348 349
		485
		502 503 549 589 598
		632 638 646 658 668
		729 747 777 788
		802		
		913 918 952 968 978
		1007 1021 1025 1037 1055 1084
		1107 1113 1158 1159 1163 1168 1187 1198
		1207 1223 1229 1236 1299
		1306 1322 1355 1394
		1432 1439 1457 1460 1464 1497
		1503 1527 1547 1561 1572 1599
		1610
		2097
	}
}
lost_isles_tradenode={
	location=2120
	color={
		120 120 120
	}
	outgoing={
		name="demacia_tradenode"
		path={
			1871 1744 1886 2030 1885 1747 2115 1632
		}
	}
	members={
		243
		488
		553
		738
		973
		1354 1379 1399
		1421 1499
		1548 1549 1550 1581 1589
		2120
	}
}
icathia_tradenode={
	location=1438
	color={
		147 92 214
	}
	outgoing={
		name="ixtal_tradenode"
		path={
			1396 1247 1435 1124 462 435 1427 563 811
		}
	}
	outgoing={
		name="belveth_tradenode"
		path={
			1125 1981
		}
	}
	members={
		128 152
		221 224
		304 354
		435 462 441 482
		544 588 596
		628
		719
		812 846
		970 987
		1124 1125
		1213 1226 1247 1258 1273
		1325 1342 1351 1396
		1427 1435 1438 1477 1487
		1609 1630 1634 1640 1643 1660 1692
	}
}
broken_isles_tradenode={
	location=2135
	color={
		232 255 101
	}
	outgoing={
		name="north_ionia_tradenode"
		path={
			2123 1148 1178 1035 1446 1493 575
		}
	}
	outgoing={
		name="central_ionia_tradenode"
		path={
			2141 2130 1307 1203
		}
	}
	outgoing={
		name="south_ionia_tradenode"
		path={
			2141 2130 2133 1108 1250
		}
	}
	members={
		284 288 232 252
		331 375
		393 398
		409
		659 660
		717 789
		898
		939 971
		1026 1036 1069 1083
		1121 1126 1148 1183
		1227 1248 1275 1282
		1305 1307 1340 1346 1372 1382
		1405 1448 1481
		1534 1537 1552 1557
		1605 1646 1652 1679 1680 1698
		2135
	}
}
eastern_great_plains_tradenode={
	location=43
	color={
		56 157 143
	}
	outgoing={
		name="great_strait_tradenode"
		path={
			62 14 306 339
		}
	}
	outgoing={
		name="western_great_plains_tradenode"
		path={
			164 258 83 235 182
		}
	}
	members={
		5 7 14 18 26 28 31 33 43 49 51 61 62 79 96
		113 129 142 174 198
		223 227
		353 369
		440 474 483
		714
		826 840
		1233 1262 
		1330 1356 1357
		1583
		1638 1644 1690

	}
}



west_freljord_tradenode={
	location=954
	color={
		198 198 198
	}
	outgoing={
		name="avarosa_tradenode"
		path={
			426 855 1215 909
		}
	}
	members={
		15 57
		146 160 183
		217 229 236 237 286 296
		376 377
		414 426 427
		535 546
		708 742 752 790
		837 855
		954
		1058 1059
		1115
		1259 1264 1276
		1312 1323 1334 1344 1363 1373
		1406 1450 1472
		1514 1528 1570 1573
		1611 1615 1629 1645 1650 1655
	}
}
east_ice_sea_coast_tradenode={
	location=1741
	color={
		255 127 0
	}
	outgoing={
		name="frostguards_tradenode"
		path={
			1219 1267 1150 312 
		}
	}
	outgoing={
		name="east_freljord_tradenode"
		path={
			1070 1153 263 606 72
		}
	}
	outgoing={
		name="avarosa_tradenode"
		path={
			720 247 1192 1204 1400
		}
	}
	members={
		225 247 263
		312
		513	522 545 558 570 571 597
		602 606 630 671
		720 730
		814 858 871 893
		1012 1016 1070
		1114 1142 1150 1153 1177 1197
		1212 1214 1219 1220 1235 1267
		1317 1371 1375
		1402 1408 1430 1431 1484 1451 1459
		1508 1519 1522 1533 1562 1567 1569
		1602 1604 1606 1625 1626 1637 1665
		1741
	}
}
frostguards_tradenode={
	location=140
	color={
		74 66 255
	}
	outgoing={
		name="east_freljord_tradenode"
		path={
			170 799 1488
		}
	}
	outgoing={
		name="dalamor_plains_tradenode"
		path={
			567 144 321 1002 616 1366 756 
		}
	}
	members={
		140 144 170
		239 294
		374 391 397
		484
		567 582 583
		629 637 685
		716 739 793 799
		856 857
		942 951
		1008
		1157
		1263 1298
		1318 1326 1331 1339 1343 1370 1380 1386
		1407 1428 1437 1441 1466
		1500 1502 1507 1510 1512 1513 1551 1597 1598
	}
}
east_freljord_tradenode={
	location=599
	color={
		0 156 255
	}
	outgoing={
		name="avarosa_tradenode"
		path={
			644 119 491 1013
		}
	}
	outgoing={
		name="tokogol_tradenode"
		path={
			866 568 1401 867 417 
		}
	}
	outgoing={
		name="dalamor_plains_tradenode"
		path={
			1117 289 259 1003 423 707
		}
	}
	members={
		72
		119
		259 260 270 277 289 290
		321 340 344
		444 472
		568 572 599
		616 643 644 667
		850 862 866 872
		975 993
		1002 1073
		1117 1151
		1237
		1488
		1531
	}
}
avarosa_tradenode={
	location=969
	color={
		0 99 97
	}
	inland=yes
	outgoing={
		name="demacia_tradenode"
		path={
			1134 762 165 804 1471
		}
	}
	outgoing={
		name="tokogol_tradenode"
		path={
			111 584 1095 828 1143 
		}
	}
	members={
		101 104 111 147
		201
		328 379
		442 491
		523 529 576 584
		623 661
		740 762
		873 888 895 896
		909 919 931 969
		1013 1045 1056 1057 1071 1074
		1134 1169 1171 1192
		1204 1215 1221
		1336
		1400 1489
		1538 1571
		1633 1651 1656 1657
	}
}
demacia_tradenode={
	location=2111
	color={
		255 254 0
	}
	outgoing={
		name="nockmirch_tradenode"
		path={
			
		}
	}
	outgoing={
		name="silent_forest_tradenode"
		path={
			193 266 439 240
		}
	}
	members={
		99
		112 165 189 193
		205 206 266
		314 323 357 358 394
		403 404 470
		520 577
		608 609 636 696 697
		703 748 795
		804 815 822 878 890
		910 972 984 991
		1049
		1109 
		1209 1217 1239 1279 1284
		1303 1327
		1458 1471 1474
		1530 1539
		1617 1619 1632 1667 1697
		1709 1710
	}
}
tokogol_tradenode={
	location=176
	color={
		63 152 0
	}
	inland=yes
	outgoing={
		name="nockmirch_tradenode"
		path={
			291 687 195 634 131
		}
	}
	outgoing={
		name="dalamor_plains_tradenode"
		path={
			
		}
	}
	members={
		176 195
		213 291
		317 325 350 383
		417 463 480
		518
		687
		803 828 867 874 886 899
		946 989
		1047 1095
		1139 1143 1154
		1283 1290
		1337 1362 1397
		1401 1473 1478
		1506 1516 1536 1563 1575
	}
}
dalamor_plains_tradenode={
	location=122
	color={
		255 38 146
	}
	outgoing={
		name="central_valoran_tradenode"
		path={
			204 248 
		}
	}
	outgoing={
		name="noxus_tradenode"
		path={
			976 900 1483 1075 
		}
	}
	members={
		122 153 171
		204 211 214 228 231 256
		318 322 332 387
		410 423 424 425 431 437 446 468 477
		506 528 531 573 581
		631 647
		707 721 741 756 758 760 761
		854 894
		930 936 960 976 988
		1003 1017 1046 1092
		1106 1129 1132 1182
		1201 1224 1274 1294
		1347 1366 1387
		1449 1482
		1504 1580 1591 1594
		1647 1688
		1700 1703 1704 1707 1724
	}
}
nockmirch_tradenode={
	location=336
	color={
		152 113 0
	}
	inland=yes
	outgoing={
		name="silent_forest_tradenode"
		path={
			226 805 876 367 569 908 449 58 1329
		}
	}
	outgoing={
		name="central_valoran_tradenode"
		path={
			226 158 961 371 453 166 249
		}
	}
	members={
		38
		118 131 158 191
		226 253 279
		336 366
		438
		532 547
		613 634 652 665 673
		702 711 794 796
		805 847 876 889
		961 996
		1039 1066 1078
		1105 1136 1145 1172 1199
		1288 1291 1292
		1314 1359
		1444 1498
		1613
	}
}
silent_forest_tradenode={
	location=1737
	color={
		116 255 18
	}
	outgoing={
		name="argent_mountains_tradenode"
		path={
			1928 2053 1990 
		}
	}
	outgoing={
		name="kalamanda_tradenode"
		path={
			1928 1963 2062
		}
	}
	members={
		27 30 58 60 71 78 80
		100 125
		238 240
		367
		439 449
		569
		662
		808
		908 922 956
		1099
		1225 1271
		1329
		1425 1479
		1576
		1668 1686 1699
		1711
	}
}
central_valoran_tradenode={
	location=319
	color={
		57 165 130
	}
	inland=yes
	outgoing={
		name="noxus_tradenode"
		path={
			1216 469 639
		}
	}
	outgoing={
		name="argent_mountains_tradenode"
		path={
			674 464 
		}
	}
	members={
		46 56 70
		132 166 184 188 199
		219 233 248 249
		319 371
		418 432 448 453 464 498
		590 591
		607 626 648 664 674 675 695
		753 754
		842
		903 983
		1022 1053 1072
		1135
		1208 1216 1222
		1463 1480
		1636
	}
}
argent_mountains_tradenode={
	location=1882
	color={
		44 71 144
	}
	outgoing={
		name="rokrund_plains_tradenode"
		path={
			429 1381 698 670 594 159
		}
	}
	outgoing={
		name="kalamanda_tradenode"
		path={
			
		}
	}
	outgoing={
		name="nashramae_tradenode"
		path={
			
		}
	}
	members={
		169
		222 250
		310 329 368 380 389
		429 433 473 487 493
		564
		618
		768 797
		818
		911 944 997
		1051 1060 1068 1079
		1173 1185 1186 1206
		1349
		1517 1582
		1607 1682
	}
}
north_ionia_tradenode={
	location=1455
	color={
		131 255 53
	}
	outgoing={
		name="central_ionia_tradenode"
		path={
			751 74 550 1419
		}
	}
	outgoing={
		name="navori_forest_tradenode"
		path={
			1188 877
		}
	}
	members={
		74
		278
		388
		401
		527 554 555 575
		706 710 751
		851 863
		1035 1065 1086
		1178 1179
		1335
		1411 1416 1418 1446 1455 1475 1493
		1509 1588
	}
}
navori_forest_tradenode={
	location=1310
	color={
		90 42 0
	}
	outgoing={
		name="central_ionia_tradenode"
		path={
			906 935 770 
		}
	}
	outgoing={
		name="noxus_tradenode"
		path={
			1302 1815 2068 1490 
		}
	}
	outgoing={
		name="northern_barrier_tradenode"
		path={
			778 953 2010 1816 1734 1076 977 1260
		}
	}
	members={
		92
		265
		365
		416
		633 686
		757 767 778 779
		836 877 885
		906	935 943 953 982
		1038 1087
		1188 1193
		1202
		1302 1310
		1422 1447
		1505
	}
}
central_ionia_tradenode={
	location=2121
	color={
		188 47 0
	}
	outgoing={
		name="south_ionia_tradenode"
		path={
			2060 1843 1358 1467
		}
	}
	outgoing={
		name="northern_barrier_tradenode"
		path={
			2136 1249 1770 155
		}
	}
	outgoing={
		name="southern_barrier_tradenode"
		path={
			722 1738 1970 732
		}
	}
	members={
		127 186
		272
		508 514 550
		672
		722 770
		852 884
		1028
		1133 1174
		1203 1238 1249
		1311 1348
		1419 1424 1443 1476
		1601 1620
	}
}
western_great_plains_tradenode={
	location=1851
	color={
		180 131 39
	}
	outgoing={
		name="camavor_tradenode"
		path={
			1743 1921
		}
	}
	outgoing={
		name="great_strait_tradenode"
		path={
			1752 1909 833 53
		}
	}
	members={
		6 8 10 11 12 19 21 29 34 35 37 39 41 48 59 63 64 66 73 83 86 89 97
		151 157 162 164 180 182 196
		209 235 242 245 251 258 285
		309 316 327 373
		445 461 476 494
		525 539 565
		902 940
		1063
		1286 1293 1297
		1341 1353 1392
		1404
		1543
		1663 1666 1678
	}
}
great_strait_tradenode={
	location=281
	color={
		23 219 0
	}
	outgoing={
		name="camavor_tradenode"
		path={
			114 1903 420 85 663
		}
	}
	members={
		24 25 32 40 44 45 52 53 68 81 95
		108 114 120 134 135 139 145
		200 267 268 281 292 297
		306 330 335 339 384 385
		471
		600 689
		833 848 859
		1009 1096
		1184
		1272 1280
		1515 1525 1555
	}
}
camavor_tradenode={
	location=1987
	color={
		87 25 147
	}
	outgoing={
		name="south_ionia_tradenode"
		path={
			1959 2020 1897 2005 1997 1960
		}
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			1866 1754 1798 1842
		}
	}
	outgoing={
		name="the_isles_tradenode"
		path={
			1921 1936 1890 1385
		}
	}
	members={
		13 17 20 82 85
		103 121 123 149 177 192
		244
		372
		420 450 458
		587 592
		640 663 699
		712 773 791 798
		800 832
		920
		1031
		1118 1175 1189 1190
		1231
		1410 1426 1434 1468
		1545
	}
}
south_ionia_tradenode={
	location=2064
	color={
		25 111 147
	}
	outgoing={
		name="southern_barrier_tradenode"
		path={
			1847 1929 994 1067 
		}
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			
		}
	}
	members={
		218 274
		419 492
		530 560 578
		603 680
		780
		907
		1050
		1108 1144 1155 1160 1194
		1200 1210 1211 1250 1268 1287
		1308 1328 1358 1376
		1440 1452 1467
		1568 1592
		1631 1635
	}
}
noxus_tradenode={
	location=1879
	color={
		255 0 0
	}
	outgoing={
		name="rokrund_plains_tradenode"
		path={
			1395 676 724
		}
	}
	outgoing={
		name="northern_barrier_tradenode"
		path={
			1809 1077 927
		}
	}
	members={
		50 75
		115 179 185
		275
		301 307 326
		402 428 447 454 469 478 481
		504 524 536 551 559
		617 635 639 650 688
		731 763 771 782
		829 830 841 875 887
		900 914 928 990
		1029 1075 1088
		1180
		1230
		1391 1395
		1412 1423 1433 1462 1483 1490
		1501 1532 1558
		1708 1716 1718
	}
}
northern_barrier_tradenode={
	location=1254
	color={
		196 190 37
	}
	outgoing={
		name="rokrund_plains_tradenode"
		path={
			1116 1269 932 853 831
		}
	}
	outgoing={
		name="southern_barrier_tradenode"
		path={
			1585 1161 1251 654 519
		}
	}
	members={
		155 197
		261
		333 338 351
		507 509 510
		651 669
		723
		823 853
		927 932 937 977
		1076 1077
		1116 1137 1161
		1205 1251 1254 1260 1265 1269
		1315 1374
		1409
		1520 1529 1553 1585 1564 1574
		1701
	}
}
southern_barrier_tradenode={
	location=168
	color={
		238 144 38
	}
	outgoing={
		name="the_cliff_tradenode"
		path={
			
		}
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			
		}
	}
	members={
		76 93
		168
		298
		342
		455 456 457 486 495 496
		505 519 556 561 593
		614 653 654
		718 732 736 764
		816 817 838
		962 994
		1014 1030 1040 1067
		1146 1164 1195
		1270
		1309 1360
		1420
		1540 1541 1554 1559 1566
		1693
	}
}
rokrund_plains_tradenode={
	location=2118
	color={
		120 32 32
	}
	outgoing={
		name="the_cliff_tradenode"
		path={
			2008 2057
		}
	}
	outgoing={
		name="belzhun_tradenode"
		path={
			2127 2069 1403 2104 
		}
	}
	outgoing={
		name="nashramae_tradenode"
		path={
			2074 2108 2037
		}
	}
	members={
		23 47 67
		138 143 159
		359
		515 594
		670 676 681 698
		724 772
		831 879 880 891
		955
		1041 1089
		1100
		1228 1277 1295
		1300 1364 1381
		1413 1415 1485
		1524 1579 1584
	}
}
kalamanda_tradenode={
	location=2114
	color={
		0 181 184
	}
	outgoing={
		name="old_shurima_tradenode"
		path={
			511 207 208 500 175 356
		}
	}
	outgoing={
		name="targon_tradenode"
		path={
			
		}
	}
	outgoing={
		name="nashramae_tradenode"
		path={
			2080 1819 1950 2078
		}
	}
	members={
		141
		207
		489
		511 574
		649 656
		725
		809
		948 965 980
		1010 1032 1090
		1103
		1245
		1338 1383
		1456 1495 1496
		1556
		1608 1621 1628
	}
}
nashramae_tradenode={
	location=2017
	color={
		176 200 0
	}
	outgoing={
		name="old_shurima_tradenode"
		path={
			
		}
	}
	outgoing={
		name="central_shurima_tradenode"
		path={
			
		}
	}
	outgoing={
		name="belzhun_tradenode"
		path={
			
		}
	}
	members={
		65
		133
		234 241
		501 537 595
		645 678 691
		785
		901 904 921 924 938 949 964 979
		1061 1091
		1281
		1361
		1417
	}
}
the_isles_tradenode={
	location=2122
	color={
		196 115 115
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			1937 1934 1873 1795
		}
	}
	outgoing={
		name="ixtal_tradenode"
		path={
			1834 1824 1901 1999 2026 2102
		}
	}
	members={
		110
		701
		1011 1052
		1110 1123
		1232 1253
		1385
		1445 1454 1492
		1521
		1600 1618 1623 1624 1662 1664 1684 1685 1691
		1728
	}
}
ixtal_tradenode={
	location=109
	color={
		116 255 18
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			
		}
	}
	outgoing={
		name="kumungu_tradenode"
		path={
			860 945 1033 1147 1048 810 605
		}
	}
	outgoing={
		name="sai_kahleek_tradenode"
		path={
			813 541 1319 540 912 512 150
		}
	}
	outgoing={
		name="belveth_tradenode"
		path={
			813 541 255 743 1170 892 386 1000 925 926
		}
	}
	members={
		22
		109 161 178
		399
		411 497
		541 552 563 566
		610
		744
		811 813 835 849 860
		916 945 957 999
		1033 1093
		1119 1140 1141 1181
		1266
		1319 1350 1377
		1523 1546
		1612 1639 1649 1669
	}
}
belveth_tradenode={
	location=1020
	color={
		108 0 200
	}
	outgoing={
		name="marai_coast_tradenode"
		path={
			2036 1971 1974 1957 1895 2059
		}
	}
	outgoing={
		name="sai_kahleek_tradenode"
		path={
			580 693 413 283 293
		}
	}
	members={
		190
		255 287
		346 386
		452 475
		540 580
		657 693 694
		743 746 776 786
		827 892
		925 926 959 992
		1000 1015 1020
		1104 1170
		1244 1255
		1345 1369
		1578
		1603
	}
}
marai_coast_tradenode={
	location=1888
	color={
		123 205 0
	}
	outgoing={
		name="central_shurima_tradenode"
		path={
			303 715 666 1054 1176 364 400 526 1034
		}
	}
	outgoing={
		name="targon_tradenode"
		path={
			
		}
	}
	members={
		84
		246 280
		303 347
		436
		642 666 684
		715 728 787 792
		845
		934 941 967 974 986
		1006 1044 1054
		1112 1176
		1256
		1301 1368 1390 1393
		1461 1470 1491
		1577 1587 1596
	}
}
targon_tradenode={
	location=1333
	color={
		140 3 210
	}
	outgoing={
		name="old_shurima_tradenode"
		path={
			
		}
	}
	members={
		91
		490
		641
		745 775
		807 861 865
		929 966
		1004 1024 1062 1098
		1241 1243
		1321 1333
		1486
		1518 1542 1544 1590
	}
}
old_shurima_tradenode={
	location=396
	color={
		144 151 75
	}
	inland=yes
	outgoing={
		name="central_shurima_tradenode"
		path={
			
		}
	}
	members={
		69 87 98
		126 175 181
		202 208 220 230 254 282
		302 356 396
		407 412 430
		500 516 517 521 562
		611 625 682
		733 734 755 766 784
		801 820 821 825 870
		917 981 985
		1018
		1122 1131 1149 1191
		1246 1261
		1389
	}
}
sai_kahleek_tradenode={
	location=1138
	color={
		250 188 31
	}
	inland=yes
	outgoing={
		name="belzhun_tradenode"
		path={
			
		}
	}
	outgoing={
		name="central_shurima_tradenode"
		path={
			
		}
	}
	outgoing={
		name="kumungu_tradenode"
		path={
			459 378 395 620 172 1101 320
		}
	}
	members={
		42 88
		136 150 167 173 187
		283 293 299
		315 352 360 362 370 378 390 395
		406 413 459
		512 538 542 548 557
		615 620 621 622 624 655 683
		759
		839 844 882 883
		912
		1019 1080 1082 1085 1094
		1102 1111 1138 1196
		1316 1384
		1627
	}
}
belzhun_tradenode={
	location=2104
	color={
		149 0 0
	}
	outgoing={
		name="the_cliff_tradenode"
		path={
			2081 2083
		}
	}
	outgoing={
		name="kumungu_tradenode"
		path={
			868 54 334 2113
		}
	}
	outgoing={
		name="central_shurima_tradenode"
		path={
			257 392 479 690 765 1097 579 269 460 679
		}
	}
	members={
		54
		106 124 172
		257
		334 392
		405 415 479
		604 619 677 690
		737 750
		824 843 868 869
		905
		1101 1156
		1234 1240 1257 1285
		1332 1367
		1403 1442 1465
		1511
		1614
	}
}
kumungu_tradenode={
	location=107
	color={
		0 120 62
	}
	outgoing={
		name="the_cliff_tradenode"
		path={
			337 130 1904
		}
	}
	outgoing={
		name="serpentine_isles_tradenode"
		path={
			273 305 1218 1165 601 1042 947 2038
		}
	}
	members={
		102 130
		262 273
		305 320 337
		467 499
		543
		605 692
		700 749 769 783
		810 834 881
		923
		1023 1048
		1127 1147 1152 1162
		1218 1252 1278 1289 1296
		1304 1365 1398
		1453 1494
	}
}



central_shurima_tradenode={
	location=77
	color={
		209 125 4
	}
	inland=yes
	members={
		77 94
		116 156 163 194
		210 212 215 269 276
		300 311 345 363 364 381 382
		400 408 421 422 434 443 451 460 465
		526 534 579
		612 679
		704 705 709 713 726 727 735 765 774 781
		806 897 
		933 950 958 995
		1001 1005 1034 1043 1064 1081 1097
		1120 1128 1130 1166 1167
		1242
		1320 1324
	}
	end=yes
}
the_cliff_tradenode={
	location=1593
	color={
		169 192 0
	}
	members={
		343
		466
		585 586
		819 864
		963
		1313 1378
		1414 1469
		1535 1560 1586 1593
		1642 1648
	}
	end=yes
}
serpentine_isles_tradenode={
	location=2126
	color={
		0 211 255
	}
	members={
		308 324 355 361
		533
		601 627
		915 947 998
		1042
		1165
		1352 1388
		1429
		1565 1595
		1616 1653 1654 1658 1659 1661 1670 1671 1672 1673 1674 1675 1676 1677 1681 1683 1687 1689 1694 1695 1696
		1702 1705 1706 1712 1713 1714 1715 1719 1720 1721 1722 1723 1725 1726 1727
	}
	end=yes
}