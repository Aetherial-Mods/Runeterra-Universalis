estate_church_build_temple_in_y = {
	can_select = {
		any_owned_province = {
			is_state_core = root
			AND = {
				NOT = { has_building = ru_temple }
				num_free_building_slots = 1
			}
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.65
			NOT = {
				any_owned_province = {
					is_state_core = root
					base_tax = 4
					AND = {
						NOT = { has_building = ru_temple }
						num_free_building_slots = 1
					}
				}
			}
		}
		modifier = {
			factor = 1.2
			any_owned_province = {
				is_state_core = root
				base_tax = 7
				AND = {
					NOT = { has_building = ru_temple }
					num_free_building_slots = 1
				}
			}
		}
		modifier = {
			factor = 2
			NOT = {
				crown_land_share = 15
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_church_build_temple_in_y_var
			value = 0
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				AND = {
					NOT = { has_building = ru_temple }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				base_tax = 4
				AND = {
					NOT = { has_building = ru_temple }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				base_tax = 7
				AND = {
					NOT = { has_building = ru_temple }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		event_target:agenda_province = {
			if = {
				limit = {
					NOT = { has_building = ru_temple }
				}
				ROOT = {
					set_variable = {
						which = estate_church_build_temple_in_y_var
						value = 1
					}
				}
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_church_build_temple_in_y_var
					value = 1
				}
			}
			event_target:agenda_province = {
				OR = {
					has_building = ru_temple
				}
			}
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_church
			loyalty = 10
		}
		if = {
			limit = {
				NOT = { crown_land_share = 15 }
				estate_territory = {
					estate = estate_church
					territory = 10
				}
			}
			take_estate_land_share_small = { estate = estate_church }
		}
		else = {
			event_target:agenda_province = {
				add_base_tax = 1
			}
		}
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_clergy_displeased
					value = 5
				}
			}
		}
		add_estate_loyalty_modifier = {
			estate = estate_church
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
		}
    }
}

estate_nobles_build_fort_building_in_y = {
	can_select = {
		any_owned_province = {
			OR = {
				AND = {
					NOT = { has_building = ru_fort_1 }
					NOT = { has_building = ru_fort_2 }
					NOT = { has_building = ru_fort_3 }
					NOT = { has_building = ru_fort_4 }
					num_free_building_slots = 1
					any_neighbor_province = {
						owner = {
							OR = {
								is_rival = root
								root = { is_rival = prev }
								AND = {
									is_subject_other_than_tributary_trigger = yes
									overlord = {
										OR = {
											is_rival = root
											root = { is_rival = prev }
										}
									}
								}
							}
						}
					}
					NOT = {
						any_neighbor_province = {
							owned_by = root
							OR = {
								has_building = ru_fort_1
								has_building = ru_fort_2
								has_building = ru_fort_3
								has_building = ru_fort_4
							}
						}
					}
				}
				AND = {
					NOT = { has_building = ru_fort_1 }
					NOT = { has_building = ru_fort_2 }
					NOT = { has_building = ru_fort_3 }
					NOT = { has_building = ru_fort_4 }
					num_free_building_slots = 1
					any_neighbor_province = {
						owned_by = root
						any_neighbor_province = {
							owner = {
								OR = {
									is_rival = root
									root = { is_rival = prev }
									AND = {
										is_subject_other_than_tributary_trigger = yes
										overlord = {
											OR = {
												is_rival = root
												root = { is_rival = prev }
											}
										}
									}
								}
							}
							NOT = {
								any_neighbor_province = {
									owned_by = root
									OR = {
										has_building = ru_fort_1
										has_building = ru_fort_2
										has_building = ru_fort_3
										has_building = ru_fort_4
									}
								}
							}
						}
					}
				}
				AND = {
					has_building = ru_fort_1
					ROOT = { mil_tech = 9 }
				}
				AND = {
					has_building = ru_fort_2
					ROOT = { mil_tech = 18 }
				}
				AND = {
					has_building = ru_fort_3
					ROOT = { mil_tech = 27 }
				}
			}
			NOT = { has_building = ru_fort_4 }
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			any_neighbor_country = {
				OR = {
					is_rival = root
					root = { is_rival = prev }
				}
				army_size = root
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_nobles_build_fort_building_in_y_var
			value = 0
		}
		random_owned_province = {
			limit = {
				OR = {
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owner = {
								OR = {
									is_rival = root
									root = { is_rival = prev }
									AND = {
										is_subject_other_than_tributary_trigger = yes
										overlord = {
											OR = {
												is_rival = root
												root = { is_rival = prev }
											}
										}
									}
								}
							}
						}
						NOT = {
							any_neighbor_province = {
								owned_by = root
								OR = {
									has_building = ru_fort_1
									has_building = ru_fort_2
									has_building = ru_fort_3
									has_building = ru_fort_4
								}
							}
						}
					}
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owned_by = root
							any_neighbor_province = {
								owner = {
									OR = {
										is_rival = root
										root = { is_rival = prev }
										AND = {
											is_subject_other_than_tributary_trigger = yes
											overlord = {
												OR = {
													is_rival = root
													root = { is_rival = prev }
												}
											}
										}
									}
								}
								NOT = {
									any_neighbor_province = {
										owned_by = root
										OR = {
											has_building = ru_fort_1
											has_building = ru_fort_2
											has_building = ru_fort_3
											has_building = ru_fort_4
										}
									}
								}
							}
						}
					}
					AND = {
						has_building = ru_fort_1
						ROOT = { mil_tech = 9 }
					}
					AND = {
						has_building = ru_fort_2
						ROOT = { mil_tech = 18 }
					}
					AND = {
						has_building = ru_fort_3
						ROOT = { mil_tech = 27 }
					}
				}
				NOT = { has_building = ru_fort_4 }
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				OR = {
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owner = {
								OR = {
									is_rival = root
									root = { is_rival = prev }
									AND = {
										is_subject_other_than_tributary_trigger = yes
										overlord = {
											OR = {
												is_rival = root
												root = { is_rival = prev }
											}
										}
									}
								}
							}
						}
						NOT = {
							any_neighbor_province = {
								owned_by = root
								OR = {
									has_building = ru_fort_1
									has_building = ru_fort_2
									has_building = ru_fort_3
									has_building = ru_fort_4
								}
							}
						}
						has_terrain = hills
					}
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owned_by = root
							any_neighbor_province = {
								owner = {
									OR = {
										is_rival = root
										root = { is_rival = prev }
										AND = {
											is_subject_other_than_tributary_trigger = yes
											overlord = {
												OR = {
													is_rival = root
													root = { is_rival = prev }
												}
											}
										}
									}
								}
								NOT = {
									any_neighbor_province = {
										owned_by = root
										OR = {
											has_building = ru_fort_1
											has_building = ru_fort_2
											has_building = ru_fort_3
											has_building = ru_fort_4
										}
									}
								}
							}
						}
						has_terrain = hills
					}
					AND = {
						has_building = ru_fort_1
						ROOT = { mil_tech = 9 }
					}
					AND = {
						has_building = ru_fort_2
						ROOT = { mil_tech = 18 }
					}
					AND = {
						has_building = ru_fort_3
						ROOT = { mil_tech = 27 }
					}
				}
				NOT = { has_building = ru_fort_4 }
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				OR = {
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owner = {
								OR = {
									is_rival = root
									root = { is_rival = prev }
									AND = {
										is_subject_other_than_tributary_trigger = yes
										overlord = {
											OR = {
												is_rival = root
												root = { is_rival = prev }
											}
										}
									}
								}
							}
						}
						NOT = {
							any_neighbor_province = {
								owned_by = root
								OR = {
									has_building = ru_fort_1
									has_building = ru_fort_2
									has_building = ru_fort_3
									has_building = ru_fort_4
								}
							}
						}
						has_terrain = mountain
					}
					AND = {
						NOT = { has_building = ru_fort_1 }
						NOT = { has_building = ru_fort_2 }
						NOT = { has_building = ru_fort_3 }
						NOT = { has_building = ru_fort_4 }
						num_free_building_slots = 1
						any_neighbor_province = {
							owned_by = root
							any_neighbor_province = {
								owner = {
									OR = {
										is_rival = root
										root = { is_rival = prev }
										AND = {
											is_subject_other_than_tributary_trigger = yes
											overlord = {
												OR = {
													is_rival = root
													root = { is_rival = prev }
												}
											}
										}
									}
								}
								NOT = {
									any_neighbor_province = {
										owned_by = root
										OR = {
											has_building = ru_fort_1
											has_building = ru_fort_2
											has_building = ru_fort_3
											has_building = ru_fort_4
										}
									}
								}
							}
						}
						has_terrain = mountain
					}
					AND = {
						has_building = ru_fort_1
						ROOT = { mil_tech = 9 }
					}
					AND = {
						has_building = ru_fort_2
						ROOT = { mil_tech = 18 }
					}
					AND = {
						has_building = ru_fort_3
						ROOT = { mil_tech = 27 }
					}
				}
				NOT = { has_building = ru_fort_4 }
			}
			save_event_target_as = agenda_province
		}
		event_target:agenda_province = {
			if = {
				limit = {
					has_building = ru_fort_3
				}
				ROOT = {
					set_variable = {
						which = estate_nobles_build_fort_building_in_y_var
						value = 4
					}
				}
			}
			else_if = {
				limit = {
					has_building = ru_fort_2
				}
				ROOT = {
					set_variable = {
						which = estate_nobles_build_fort_building_in_y_var
						value = 3
					}
				}
			}
			else_if = {
				limit = {
					has_building = ru_fort_1
				}
				ROOT = {
					set_variable = {
						which = estate_nobles_build_fort_building_in_y_var
						value = 2
					}
				}
			}
			else = {
				ROOT = {
					set_variable = {
						which = estate_nobles_build_fort_building_in_y_var
						value = 1
					}
				}
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_nobles_build_fort_building_in_y_var
					value = 4
				}
			}
			event_target:agenda_province = {
				has_building = ru_fort_4
			}
		}
		else_if = {
			limit = {
				check_variable = {
					which = estate_nobles_build_fort_building_in_y_var
					value = 3
				}
			}
			event_target:agenda_province = {
				OR = {
					has_building = ru_fort_3
					has_building = ru_fort_4
				}
			}
		}
		else_if = {
			limit = {
				check_variable = {
					which = estate_nobles_build_fort_building_in_y_var
					value = 2
				}
			}
			event_target:agenda_province = {
				OR = {
					has_building = ru_fort_2
					has_building = ru_fort_3
					has_building = ru_fort_4
				}
			}
		}
		else = {
			event_target:agenda_province = {
				OR = {
					has_building = ru_fort_1
					has_building = ru_fort_2
					has_building = ru_fort_3
					has_building = ru_fort_4
				}
			}
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		event_target:agenda_province = {
			add_base_manpower = 1
		}
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_nobles_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_nobles
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_burghers_build_trade_building_in_y = {
	can_select = {
		any_owned_province = {
			is_state_core = root
			AND = {
				NOT = { has_building = ru_marketplace }
				num_free_building_slots = 1
			}
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.65
			NOT = {
				any_owned_province = {
					is_state_core = root
					OR = {
						province_has_center_of_trade_of_level = 1
						province_trade_power = 10
					}
					AND = {
						NOT = { has_building = ru_marketplace }
						num_free_building_slots = 1
					}
				}
			}
		}
		modifier = {
			factor = 1.2
			any_owned_province = {
				is_state_core = root
				OR = {
					province_has_center_of_trade_of_level = 2
					province_trade_power = 20
				}
				AND = {
					NOT = { has_building = ru_marketplace }
					num_free_building_slots = 1
				}
			}
		}
		modifier = {
			factor = 2
			NOT = {
				crown_land_share = 15
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_burghers_build_trade_building_in_y_var
			value = 0
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				AND = {
					NOT = { has_building = ru_marketplace }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				OR = {
					province_has_center_of_trade_of_level = 1
					province_trade_power = 10
				}
				AND = {
					NOT = { has_building = ru_marketplace }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				OR = {
					province_has_center_of_trade_of_level = 2
					province_trade_power = 20
				}
				AND = {
					NOT = { has_building = ru_marketplace }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		event_target:agenda_province = {
			if = {
				limit = {
					NOT = { has_building = ru_marketplace }
				}
				ROOT = {
					set_variable = {
						which = estate_burghers_build_trade_building_in_y_var
						value = 1
					}
				}
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_burghers_build_trade_building_in_y_var
					value = 1
				}
			}
			event_target:agenda_province = {
				has_building = ru_marketplace
			}
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = 10
		}
		if = {
			limit = {
				NOT = { crown_land_share = 15 }
				estate_territory = {
					estate = estate_burghers
					territory = 10
				}
			}
			take_estate_land_share_small = { estate = estate_burghers }
		}
		else = {
			event_target:agenda_province = {
				add_base_production = 1
			}
		}
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_burghers_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_burghers
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_burghers_build_port_building_in_y = {
	can_select = {
		any_owned_province = {
			is_state_core = root
			has_port = yes
			AND = {
				NOT = { has_building = ru_shipyard }
				num_free_building_slots = 1
			}
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.75
			always = yes
		}
		modifier = {
			factor = 2
			NOT = {
				crown_land_share = 15
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_burghers_build_port_building_in_y_var
			value = 0
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				has_port = yes
				AND = {
					NOT = { has_building = ru_shipyard }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				development = 12
				has_port = yes
				AND = {
					NOT = { has_building = ru_shipyard }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				development = 20
				has_port = yes
				AND = {
					ROOT = { dip_tech = 8 }
					NOT = { has_building = ru_shipyard }
					num_free_building_slots = 1
				}
			}
			save_event_target_as = agenda_province
		}
		event_target:agenda_province = {
			if = {
				limit = {
					NOT = { has_building = ru_shipyard }
				}
				ROOT = {
					set_variable = {
						which = estate_burghers_build_port_building_in_y_var
						value = 1
					}
				}
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_burghers_build_port_building_in_y_var
					value = 1
				}
			}
			event_target:agenda_province = {
				has_building = ru_shipyard
			}
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = 10
		}
		if = {
			limit = {
				NOT = { crown_land_share = 15 }
				estate_territory = {
					estate = estate_burghers
					territory = 10
				}
			}
			take_estate_land_share_small = { estate = estate_burghers }
		}
		else = {
			event_target:agenda_province = {
				add_base_production = 1
			}
		}
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_burghers_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_burghers
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_burghers_build_production_building_in_y = {
	can_select = {
		any_owned_province = {
			is_state_core = root
			OR = {
				AND = {
					NOT = { has_building = ru_mine }
					num_free_building_slots = 1
				}
			}
			NOT = { trade_goods = gold }
		}
	}
	selection_weight = {
		factor = 1
		modifier = {
			factor = 0.65
			NOT = {
				any_owned_province = {
					is_state_core = root
					base_production = 4
					AND = {
						NOT = { has_building = ru_mine }
						num_free_building_slots = 1
					}
					NOT = { trade_goods = gold }
				}
			}
		}
		modifier = {
			factor = 1.2
			any_owned_province = {
				is_state_core = root
				OR = {
					base_production = 7
					has_building = ru_manufactory
				}
				AND = {
					NOT = { has_building = ru_mine }
					num_free_building_slots = 1
				}
				NOT = { trade_goods = gold }
			}
		}
		modifier = {
			factor = 2
			NOT = {
				crown_land_share = 15
			}
		}
	}
	pre_effect = {
		set_variable = {
			which = estate_burghers_build_production_building_in_y_var
			value = 0
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				AND = {
					NOT = { has_building = ru_mine }
					num_free_building_slots = 1
				}
				NOT = { trade_goods = gold }
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				base_production = 4
				AND = {
					NOT = { has_building = ru_mine }
					num_free_building_slots = 1
				}
				NOT = { trade_goods = gold }
			}
			save_event_target_as = agenda_province
		}
		random_owned_province = {
			limit = {
				is_state_core = root
				OR = {
					base_production = 7
					has_building = ru_manufactory
				}
				AND = {
					NOT = { has_building = ru_mine }
					num_free_building_slots = 1
				}
				NOT = { trade_goods = gold }
			}
			save_event_target_as = agenda_province
		}
		event_target:agenda_province = {
			if = {
				limit = {
					NOT = { has_building = ru_mine }
				}
				ROOT = {
					set_variable = {
						which = estate_burghers_build_production_building_in_y_var
						value = 1
					}
				}
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		if = {
			limit = {
				check_variable = {
					which = estate_burghers_build_production_building_in_y_var
					value = 1
				}
			}
			event_target:agenda_province = {
				has_building = ru_mine
			}
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = 10
		}
		if = {
			limit = {
				NOT = { crown_land_share = 15 }
				estate_territory = {
					estate = estate_burghers
					territory = 10
				}
			}
			take_estate_land_share_small = { estate = estate_burghers }
		}
		else = {
			event_target:agenda_province = {
				add_base_production = 1
			}
		}
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_burghers_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_burghers
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_church_develop_x = {
	selection_weight = {
		factor = 1
	}
	can_select = {
		capital_scope = {
			is_state_core = root
		}
	}
	pre_effect = {
		random_list = {
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
						area_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			2 = {
				trigger = {
					any_owned_province = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
				}
				random_owned_province = {
					limit = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
					}
					save_event_target_as = agenda_province
				}
			}
		}
		event_target:agenda_province = {
			if = {
				limit = {
					check_variable = {
						which = province_adm_var
						value = 1
					}
				}
				set_variable = {
					which = province_adm_var
					value = 0
				}
			}
			export_to_variable = {
				which = province_adm_var
				value = trigger_value:base_tax
			}
			change_variable = {
				which = province_adm_var
				value = 2
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		event_target:agenda_province = {
			base_tax = "province_adm_var"
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_church
			loyalty = 10
		}
		add_prestige = 10
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_clergy_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_church
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_nobles_develop_x = {
	selection_weight = {
		factor = 1
	}
	can_select = {
		capital_scope = {
			is_state_core = root
		}
	}
	pre_effect = {
		random_list = {
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
						area_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			2 = {
				trigger = {
					any_owned_province = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
				}
				random_owned_province = {
					limit = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
					}
					save_event_target_as = agenda_province
				}
			}
		}
		event_target:agenda_province = {
			if = {
				limit = {
					check_variable = {
						which = province_mil_var
						value = 1
					}
				}
				set_variable = {
					which = province_mil_var
					value = 0
				}
			}
			export_to_variable = {
				which = province_mil_var
				value = trigger_value:base_manpower
			}
			change_variable = {
				which = province_mil_var
				value = 2
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		event_target:agenda_province = {
			base_manpower = "province_mil_var"
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		 add_yearly_manpower = 0.3
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_nobles_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_nobles
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}

estate_burghers_develop_x = {
	selection_weight = {
		factor = 1
	}
	can_select = {
		capital_scope = {
			is_state_core = root
		}
	}
	pre_effect = {
		random_list = {
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
						area_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			2 = {
				trigger = {
					any_owned_province = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
				}
				random_owned_province = {
					limit = {
						is_state_core = root
						OR = {
							AND = {
								religion = root
								OR = {
									culture = root
									has_owner_accepted_culture = yes
								}
							}
						}
						region_for_scope_province = {
							is_capital_of = root
						}
					}
					save_event_target_as = agenda_province
				}
			}
			1 = {
				random_owned_province = {
					limit = {
						is_state_core = root
					}
					save_event_target_as = agenda_province
				}
			}
		}
		event_target:agenda_province = {
			if = {
				limit = {
					check_variable = {
						which = province_dip_var
						value = 1
					}
				}
				set_variable = {
					which = province_dip_var
					value = 0
				}
			}
			export_to_variable = {
				which = province_dip_var
				value = trigger_value:base_production
			}
			change_variable = {
				which = province_dip_var
				value = 2
			}
		}
	}
	provinces_to_highlight = {
		province_id = event_target:agenda_province
	}
	fail_if = {
		event_target:agenda_province = {
			NOT = { owned_by = root }
		}
	}
    task_requirements = {
		event_target:agenda_province = {
			base_production = "province_dip_var"
		}
    }
    task_completed_effect = {
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = 10
		}
		add_years_of_income = 0.3
    }
    failing_effect = {
		event_target:agenda_province = {
			if = {
				limit = { owned_by = root }
				add_named_unrest = {
					name = local_burghers_displeased
					value = 5
				}
			}
		}
        add_estate_loyalty_modifier = {
            estate = estate_burghers
			desc = EST_VAL_AGENDA_DENIED
			loyalty = -5
			duration = 7300
        }
    }
}