naval_hegemon = {
	allow = {
		is_great_power = yes
		navy_size = 250
		NOT = { any_other_great_power = { navy_size = root } }
		NOT = { has_country_modifier = lost_hegemony }
	}
	
	base = {
		free_dip_policy = 1
		monarch_diplomatic_power = 1
		war_exhaustion = -0.1
		global_spy_defence = 0.25
		global_naval_engagement_modifier = 0.10
	}
	
	scale = {
		reduced_liberty_desire = 20
		global_sailors_modifier = 2.0
		blockade_efficiency = 2.0
	}
	
	max = {
		backrow_artillery_damage = 0.20
	}
}