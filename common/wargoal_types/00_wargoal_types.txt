# INSTRUCTIONS:
# -------------
# prestige_factor - Multiplied with any prestige increase normally associated with a peace option.
# transfer_trade_cost_factor - Multiplied with the cost for transfering trade power.
# peace_cost_factor - Multiplied with the cost of the peace options in the peace treaty
# po_xxx - Peace options. If toggled on, badboy_factor and prestige_factor are multiplied with any associated base changes to these (see defines.txt.)
# allowed_provinces - If 'po_demand_provinces' is on, badboy_factor applies to these provinces.
# allow_annex - Always allow annexation (even if other rules disallow)
# deny_annex - Always deny annexation (even if other rules allows it)
#
# ROOT = attacker
# FROM = target
#
# Place peace offers in attacker and/or defender blocks
# The peace options are:
# po_demand_provinces
# po_revoke_cores
# po_release_vassals
# po_release_annexed
# po_change_religion
# po_form_personal_union
# po_gold
# po_become_vassal
# po_subjugate_vassal (same as become vassal, but for subjugate CB so it's always available and for a fixed war score)
# po_concede_defeat
# po_annul_treaties
# po_change_government
# po_revoke_elector
# po_trade_power
# po_steer_trade
# po_humiliate
# po_enforce_rebel_demands
#
# NOTE: The order in which the peace options are listed is the order in which the AI will normally prioritize them in peace treaties
#
# WAR GOAL TYPES:
#	- take_colony
#	- take_core
#	- take_border
#	- take_province
#	- take_capital
#	- defend_capital
#	- defend_country
#	- naval_superiority
#	- superiority
#	- take_region
#	- blockade_ports
#
# --------------------------------------------------------------

take_capital_shuriman_civilwar = {
	type = take_capital
	
	attacker = 	{
		badboy_factor = 1
		prestige_factor = 1
		peace_cost_factor = 1

		peace_options = {
			po_form_personal_union
		}
	}
	
	defender = 	{
		badboy_factor = 0.25
		prestige_factor = 1
		peace_cost_factor = 0.75

		allowed_provinces = {
			is_core = FROM
		}
		
		peace_options = {
			po_demand_provinces
			po_return_cores
		}
	}
	
	war_name = SHURIMAN_CIVILWAR_NAME
}

take_border_void_invasion = {
	type = take_border
	
	attacker = {
		badboy_factor = 1.25
		prestige_factor = 1
		peace_cost_factor = 1
	
		peace_options = {
			po_demand_provinces
			po_change_religion
		}
	}
	
	defender = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 1
	
		peace_options = {
			po_demand_provinces
			po_change_religion
		}
	}
	
	war_name = VOID_INVASION_NAME
}

take_capital_push_back_the_void = {
	type = take_capital
	
	attacker = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 1
	
		peace_options = {
			po_demand_provinces
			po_change_religion
		}
	}
	
	defender = {
		badboy_factor = 1.25
		prestige_factor = 1
		peace_cost_factor = 1
	
		peace_options = {
			po_demand_provinces
			po_change_religion
		}
	}
	
	war_name = PUSH_BACK_THE_VOID_NAME
}

take_core = {
	type = take_core

	attacker = {
		badboy_factor = 0.25
		prestige_factor = 1
		peace_cost_factor = 0.75

		allowed_provinces = {
			is_core = ROOT
		}
		
		peace_options = {
			po_demand_provinces
			po_return_cores
		}
		
		prov_desc = ALL_CORES
	}
	
	defender = {
		badboy_factor = 0.25
		prestige_factor = 1
		peace_cost_factor = 0.75

		allowed_provinces = {
			is_core = FROM
		}
		
		peace_options = {
			po_demand_provinces
			po_return_cores
		}
		
		prov_desc = ALL_CORES	
	}
	
	war_name = CORE_WAR_NAME
}

take_claim = {
	type = take_province
	
	attacker = {
		badboy_factor = 1.0
		prestige_factor = 1
		peace_cost_factor = 1.0	
		allowed_provinces = {
			is_claim = ROOT
		}
		peace_options = {
			po_demand_provinces
		}
		prov_desc = ALL_CLAIMS
	}
	
	defender = {
		badboy_factor = 1.0
		prestige_factor = 1
		peace_cost_factor = 1.0	
		allowed_provinces = {
			OR = {
				is_claim = FROM
				is_core = FROM
			}
		}
		peace_options = {
			po_demand_provinces
		}
		prov_desc = ALL_CLAIMS
	}
	war_name = CLAIM_WAR_NAME
}

defend_capital_independence = {
	type = defend_capital
	
	attacker = {
		badboy_factor = 1
		prestige_factor = 1
		peace_cost_factor = 0.75
		
		peace_options = {
			po_concede_defeat
			po_independence
		}
	}
	
	defender = {
		badboy_factor = 0.5
		prestige_factor = 1
		peace_cost_factor = 0.75
		
		allowed_provinces = {
			always = yes
		}		
		
		peace_options = {
			po_demand_provinces
		}
	}
	
	war_name = INDEPENDENCE_WAR_NAME
}

take_capital_imperial = {
	type = take_capital
	
	attacker = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 0.75	
		
		
		allowed_provinces = {
			always = yes
		}
		
		peace_options = {
			po_demand_provinces
		}
		
		country_desc = ALL_COUNTRIES
		prov_desc = ALL_PROVS
	}
	
	defender = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 0.75	
		
		
		allowed_provinces = {
			always = yes
		}
		
		peace_options = {
			po_demand_provinces
		}
		
		country_desc = ALL_COUNTRIES
		prov_desc = ALL_PROVS
	}	
	
	war_name = IMPERIAL_WAR_NAME
}

superiority_trade_mutual = {
	type = superiority
	
	attacker = {
		badboy_factor = 1
		prestige_factor = 2
		peace_cost_factor = 0.75	
		
		peace_options = {
			po_revoke_cores
			po_gold
			po_concede_defeat
			po_trade_power
			po_steer_trade
		}
	}
	
	defender = {
		badboy_factor = 1
		prestige_factor = 2
		peace_cost_factor = 0.75	
		
		peace_options = {
			po_revoke_cores
			po_gold
			po_concede_defeat
			po_trade_power
			po_steer_trade
		}
	}
	
	
	war_name = TRADE_WAR_NAME	
}

superiority_punitive = {
	type = superiority
	
	badboy_factor = 0.1
	prestige_factor = 1
	peace_cost_factor = 0.75	
	
	allowed_provinces = {
		is_core = ROOT
	}
	
	peace_options = {
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_demand_provinces
		po_revoke_cores
		po_gold
		po_concede_defeat
	}
	
	
	prov_desc = ALL_CORES
	war_name = PUNITIVE_WAR_NAME	
}

superiority_horde = {
	type = superiority
	
	attacker = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 1	
		
		allowed_provinces = {
			always = yes
		}

		peace_options = {
			po_demand_provinces
			po_become_vassal
			po_become_tributary_state
			po_gold
		}
		
		prov_desc = ALL_PROVS
	}
	
	defender = {
		badboy_factor = 0.75
		prestige_factor = 1
		peace_cost_factor = 1	
		
		allowed_provinces = {
			always = yes
		}

		peace_options = {
			po_demand_provinces
			po_become_vassal
			po_become_tributary_state
			po_gold
		}
		
		prov_desc = ALL_PROVS
	}	
	
	
	war_name = PRIMITIVE_WAR_NAME	
}

take_capital_throne = {
	type = take_capital
	
	attacker = {
		badboy_factor = 1
		prestige_factor = 2
		peace_cost_factor = 1.4
		
		allowed_provinces = {
			always = yes
		}

		peace_options = {
			po_form_personal_union
		}
	}
	
	defender = {
		badboy_factor = 0.5
		prestige_factor = 1
		peace_cost_factor = 0.75

		allowed_provinces = {
			is_core = FROM
		}
		
		peace_options = {
			po_demand_provinces
		}
	}
	
	prov_desc = ALL_PROVS
	war_name = THRONE_WAR_NAME	
}

take_capital_support_rebels = {
	type = take_capital
	
	attacker = {
		peace_options = {
			po_enforce_rebel_demands
		}
	}
	
	badboy_factor = 1
	prestige_factor = 2
	peace_cost_factor = 0.5
	
	war_name = SUPPORT_REBELS_WAR_NAME
}

humiliate_rotw = {
	type = take_capital
	
	badboy_factor = 1
	prestige_factor = 1.25
	peace_cost_factor = 1
	
	allowed_provinces = {
		always = no
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_gold
		po_annul_treaties
		po_trade_power
		po_steer_trade
		po_humiliate
		po_humiliate_rival
	}
	
	deny_annex = yes
	
	country_desc = ALL_COUNTRIES
	prov_desc = NO_PROVS
	war_name = HUMILIATE_ROTW_WAR_NAME	
}