# New personalities cannot be added to this file
# Do not change existing personality names!

human = {
	chance = {
		factor = 0
	}
	
	icon = 1
}

ai_capitalist = {
	chance = {
		factor = 1
		modifier = {
			factor = 2
			adm = 1
		}			
		modifier = {
			factor = 2
			adm = 2
		}
		modifier = {
			factor = 2
			adm = 3
		}	
		modifier = {
			factor = 2
			adm = 4
		}		
		modifier = {
			factor = 2
			adm = 5
		}		
		modifier = {
			factor = 2
			adm = 6
		}
	}
	icon = 2
}

ai_diplomat = {
	chance = {
		factor = 1
		modifier = {
			factor = 2
			dip = 1
		}			
		modifier = {
			factor = 2
			dip = 2
		}
		modifier = {
			factor = 2
			dip = 3
		}	
		modifier = {
			factor = 2
			dip = 4
		}		
		modifier = {
			factor = 2
			dip = 5
		}		
		modifier = {
			factor = 2
			dip = 6
		}
		modifier = {
			factor = 2
			is_emperor = yes
		}
		modifier = {
			factor = 2
			is_emperor = yes
			total_development = 50
		}
		modifier = {
			factor = 2
			is_emperor = yes
			total_development = 100
		}
	}
	icon = 3
}

ai_militarist = {
	chance = {
		factor = 1
		modifier = {
			factor = 0
			has_regency = yes
		}
		modifier = {
			factor = 2
			mil = 1
		}			
		modifier = {
			factor = 2
			mil = 2
		}
		modifier = {
			factor = 2
			mil = 3
		}	
		modifier = {
			factor = 2
			mil = 4
		}		
		modifier = {
			factor = 2
			mil = 5
		}		
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
			total_development = 50
		}
		modifier = {
			factor = 0.5
			is_emperor = yes
			total_development = 100
		}
		modifier = {
			factor = 10
			tag = WNC
		}
		modifier = {
			factor = 10
			tag = TRC
		}
		modifier = {
			factor = 10
			technology_group = void
		}
	}
	icon = 4
}

ai_colonialist = {
	chance = {
		factor = 1	
		modifier = {
			factor = 2
			adm = 1
		}			
		modifier = {
			factor = 2
			adm = 2
		}
		modifier = {
			factor = 2
			adm = 3
		}	
		modifier = {
			factor = 2
			adm = 4
		}		
		modifier = {
			factor = 2
			adm = 5
		}		
		modifier = {
			factor = 2
			adm = 6
		}	
		modifier = {
			factor = 0
			NOT = { num_of_colonists = 1 }
		}
		modifier = {
			factor = 10
			tag = URS
		}
		modifier = {
			factor = 10
			OR = {
				tag = SHU
				tag = GSH
			}
		}
	}
	icon = 5
}

ai_balanced = {
	chance = {
		factor = 1
		modifier = {
			factor = 0
			has_regency = yes
		}
		modifier = {
			factor = 2
			adm = 1
			dip = 1
			mil = 1
		}
		modifier = {
			factor = 2
			adm = 2
			dip = 2
			mil = 2
		}
		modifier = {
			factor = 2
			adm = 3
			dip = 3
			mil = 3
		}	
		modifier = {
			factor = 2
			adm = 4
			dip = 4
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 5
			dip = 5
			mil = 5
		}		
		modifier = {
			factor = 2
			adm = 6
			dip = 6
			mil = 6
		}
	}
	icon = 6
}