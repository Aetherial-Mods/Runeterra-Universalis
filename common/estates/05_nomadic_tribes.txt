estate_nomadic_tribes = {
	icon = 5

	# If true, country will get estate
	trigger = {
		NOT = { has_reform = godhood_reform }
		government = tribal
	}

	# These scale with loyalty & power
	country_modifier_happy = {
		cavalry_cost = -0.2
		manpower_recovery_speed = 0.2
	}
	country_modifier_neutral = {
		manpower_recovery_speed = 0.2
	}	
	country_modifier_angry = {
		horde_unity = -4
		manpower_recovery_speed = -0.4
		raze_power_gain = -1
		global_unrest = 2
	}
	land_ownership_modifier = {
		nomadic_tribes_loyalty_modifier = 0.2
	}

	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 2
			has_terrain = steppe
		}
		modifier = {
			factor = 2
			culture_group = owner
		}
		modifier = {
			factor = 0.5
			NOT = { religion = owner }
			NOT = { culture_group = owner }
		}
		modifier = {
			factor = 1.5
			NOT = { development = 10 }
		}
		modifier = {
			factor = 0.75
			development = 15
		}
		modifier = {
			factor = 0.75
			development = 20
		}
	}

	# Influence modifiers
	base_influence = 25

	influence_from_dev_modifier = 0.4

	privileges = {
		estate_nomadic_tribes_land_rights
		estate_nomadic_tribes_horde_power
		estate_nomadic_tribes_chieftains_autonomy
		estate_nomadic_tribes_advisors
		estate_nomadic_tribes_tribal_host
		estate_nomadic_tribes_guaranteed_leadgership_in_host
	}

	agendas = {
		estate_nomadic_tribes_develop_x
	}

	color = { 50 150 50 }
}