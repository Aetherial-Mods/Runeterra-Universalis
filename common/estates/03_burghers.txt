estate_burghers =
{
	icon = 3

	# If true, country will get estate
	trigger = {
		NOT = { religion = spirit_blossom }
		NOT = { has_reform = godhood_reform }
		NOT = { has_reform = steppe_horde }
		NOT = { government = tribal }
	}

	# These scale with loyalty & power
	country_modifier_happy = {
		trade_efficiency = 0.2
		development_cost = -0.1
	}
	country_modifier_neutral = {
		trade_efficiency = 0.2
	}	
	country_modifier_angry = {
		trade_efficiency = -0.1
		development_cost = 0.1
		global_unrest = 2
	}
	land_ownership_modifier = {
		burghers_loyalty_modifier = 0.2
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 0.1
			NOT = { development = 5 }
		}
		modifier = {
			factor = 0.5
			OR = {
				NOT = { development = 10 }
				NOT = { province_trade_power = 5 }
			}
		}
		modifier = {
			factor = 2
			development = 20
		}
		modifier = {
			factor = 0.5
			is_territorial_core = owner
		}
		modifier = {
			factor = 1.5
			province_trade_power = 10
		}
		modifier = {
			factor = 1.5
			province_trade_power = 20
		}
	}

	# Influence modifiers
	base_influence = 10
	influence_modifier = {	
		desc = EST_HIGH_DEVELOPMENT_PROVINCE
		trigger = {
			any_owned_province = {
				development = 30
			}
		}
		influence = 5
	}
	influence_modifier = {	
		desc = EST_VAL_COT
		trigger = {
			any_owned_province = {
				province_has_center_of_trade_of_level = 1
			}
		}
		influence = 5
	}
	influence_modifier = {
		desc = EST_VAL_LOW_TRADE_INCOME
		trigger = {
			NOT = { trade_income_percentage = 0.25 }
		}	
		influence = -5
	}
	influence_modifier = {
		desc = EST_VAL_HIGH_TRADE_INCOME
		trigger = {
			trade_income_percentage = 0.5
			NOT = { trade_income_percentage = 0.75 }
		}	
		influence = 5
	}
	influence_modifier = {
		desc = EST_VAL_VERY_HIGH_TRADE_INCOME
		trigger = {
			trade_income_percentage = 0.75
		}	
		influence = 10
	}
	influence_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER
		trigger = {
			OR = {
				has_disaster = estate_nobility_disaster
				has_disaster = estate_church_disaster
			}
		}	
		influence = -40
	}
	loyalty_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER_LOY
		trigger = {
			OR = {
				has_disaster = estate_nobility_disaster
				has_disaster = estate_church_disaster
			}
		}
		loyalty = -20
	}

	color = { 35 35 150 }

	privileges = {
		estate_burghers_land_rights
		estate_burghers_land_of_commerce
		estate_burghers_free_enterprise
		estate_burghers_commercial_board_of_advice
		estate_burghers_private_trade_fleets
		estate_burghers_admirals
		estate_burghers_patronage_of_the_arts
		estate_burghers_indebted_to_burghers
		estate_burghers_monopoly_of_textiles
		estate_burghers_monopoly_of_glass
		estate_burghers_monopoly_of_paper
	}

	agendas = {
		estate_burghers_develop_x
		estate_burghers_build_trade_building_in_y
		estate_burghers_build_port_building_in_y
	}

	influence_from_dev_modifier = 1.0
}