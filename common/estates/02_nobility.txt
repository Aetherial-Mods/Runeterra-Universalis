estate_nobles =
{
	icon = 2

	# If true, country will get estate
	trigger = {
		NOT = { religion = spirit_blossom }
		NOT = { has_reform = godhood_reform }
		NOT = { has_reform = steppe_horde }
		NOT = { government = tribal }
	}

	country_modifier_happy = {
		manpower_recovery_speed = 0.2 
		land_maintenance_modifier = -0.1
		monthly_support_heir_gain = 0.1
	}
	country_modifier_neutral = {
		manpower_recovery_speed = 0.2 
	}	
	country_modifier_angry = {
		manpower_recovery_speed = -0.1
		land_maintenance_modifier = 0.1
		global_unrest = 2
	}
	land_ownership_modifier = {
		nobles_loyalty_modifier = 0.2
	}

	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}

	# Influence modifiers
	base_influence = 10
	influence_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER
		trigger = {
			OR = {
				has_disaster = estate_church_disaster
				has_disaster = estate_burghers_disaster
			}
		}	
		influence = -40
	}
	
	loyalty_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER_LOY
		trigger = {
			OR = {
				has_disaster = estate_church_disaster
				has_disaster = estate_burghers_disaster
			}
		}
		loyalty = -20
	}
	loyalty_modifier = {
		desc = EST_VAL_NOBLE_CONSORT
		trigger = {
			has_dlc = "Rights of Man"
			has_consort = yes
			is_origin_of_consort = ROOT
		}
		loyalty = 5
	}
	
	color = { 200 0 50 }
	
	privileges = {
		estate_nobles_land_rights
		estate_nobles_nobility_primacy
		estate_nobles_right_of_counsel
		estate_nobles_advisors
		estate_nobles_strong_duchies
		estate_nobles_supremacy_over_crown
		estate_nobles_officer_corp
		estate_nobles_monopoly_of_metals
		estate_nobles_monopoly_of_gems
	}

	agendas = {
		estate_nobles_develop_x
		estate_nobles_build_fort_building_in_y
	}
	
	influence_from_dev_modifier = 1.0
}