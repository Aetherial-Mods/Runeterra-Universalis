# Runeterra Universalis
This is the repository for the Europa Universalis IV mod Runeterra Universalis, a complete overhaul of the game to have content and lore from the League of Legends Universe.

## Download Links (Mod is still in Alpha stage)
* [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2623419485)
* [Paradox Mods](https://mods.paradoxplaza.com/mods/65360/Any)
* [Codeberg (Direct Download)](https://codeberg.org/Aetherial-Mods/Runeterra-Universalis/releases)