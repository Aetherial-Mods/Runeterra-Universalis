country_decisions = {

	kumungu_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_kumungu_flag }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { tag = KUM }
			capital_scope = {
				OR = {
					region = kumungu_region	
					region = ixtal_region
					area = serpentine_isles_area_1
					area = serpentine_isles_area_2
					area = serpentine_isles_area_3
					area = serpentine_isles_area_4
				}
			}
			primary_culture = kumungian
		}
		provinces_to_highlight = {
			OR = {
				region = kumungu_region
				province_id = 107
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = KUM }
			NOT = { government = native }
			primary_culture = kumungian
			owns_core_province = 107
			num_of_owned_provinces_with = {
				is_core = ROOT
				region = kumungu_region
				value = 10
			}
		}
		effect = {
			change_tag = KUM
			set_government_rank = 2
			107 = {
				move_capital_effect = yes
			}
			kumungu_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = KUM
			}
			if = {
				limit = {
					NOT = { has_idea_group = KUM_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_kumungu_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}