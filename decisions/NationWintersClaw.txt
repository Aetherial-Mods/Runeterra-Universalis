country_decisions = {

	winters_claw_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_winters_claw_flag }
			NOT = { tag = AVA }
			NOT = { tag = WNC }
			NOT = { tag = FRO }
			NOT = { tag = TIC }
			NOT = { tag = FRE }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			primary_culture = barbarian
			religion = freljordian_pantheon
			capital_scope = {
				superregion = freljord_superregion
			}
		}
		provinces_to_highlight = {
			OR = {
				region = east_ice_sea_coast_region
				province_id = 1070
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_subject = no
			is_at_war = no
			NOT = { exists = WNC }
			NOT = { exists = FRE }
			owns_core_province = 1070
			num_of_owned_provinces_with = {
				is_core = ROOT
				region = east_ice_sea_coast_region
				value = 7
			}
		}
		effect = {
			change_tag = WNC
			swap_non_generic_missions = yes
			if = {
				limit = {
					NOT = { has_idea_group = WNC_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_winters_claw_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}