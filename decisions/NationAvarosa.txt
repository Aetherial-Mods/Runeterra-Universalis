country_decisions = {

	avarosan_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_avarosa_flag }
			NOT = { tag = AVA }
			NOT = { tag = WNC }
			NOT = { tag = FRO }
			NOT = { tag = TIC }
			NOT = { tag = FRE }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			primary_culture = avarosan
			religion = freljordian_pantheon
			capital_scope = {
				superregion = freljord_superregion
			}
		}
		provinces_to_highlight = {
			OR = {
				area = west_freljord_area_1
				area = west_freljord_area_2
				region = avarosa_region
				province_id = 969
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = AVA }
			NOT = { exists = FRE }
			owns_core_province = 969
			num_of_owned_provinces_with = {
				is_core = ROOT
				OR = {
					area = west_freljord_area_1
					area = west_freljord_area_2
					region = avarosa_region
				}
				value = 20
			}
		}
		effect = {
			change_tag = AVA
			swap_non_generic_missions = yes
			if = {
				limit = {
					NOT = { has_idea_group = AVA_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_avarosa_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}