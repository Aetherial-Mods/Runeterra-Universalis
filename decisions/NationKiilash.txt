country_decisions = {

	killash_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_killash_flag }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { tag = KIL }
			OR = {
				primary_culture = kiilash
				accepted_culture = kiilash
			}
			capital_scope = {
				OR = {
					region = ixtal_region
					region = sai_kahleek_region
					region = belveth_region
				}
			}
		}
		provinces_to_highlight = {
			province_id = 883
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_at_war = no
			NOT = { exists = KIL }
			NOT = { num_of_cities = 4 }
			OR = {
				primary_culture = kiilash
				accepted_culture = kiilash
			}
			owns_core_province = 883
		}
		effect = {
			change_tag = KIL
			set_government_rank = 1
			if = {
				limit = {
					NOT = { has_idea_group = KIL_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_killash_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}