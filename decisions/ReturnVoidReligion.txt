country_decisions = {

	return_void_religion = {
		major = yes
		potential = {
			technology_group = void
			NOT = { religion = the_watchers }
			has_country_flag = broken_void_flag
		}
		allow = {
			stability = 3
			num_of_religion = {
				religion = the_watchers
				value = 0.5
			}
		}
		effect = {
			add_stability = -3
			change_religion = the_watchers
		}
		ai_will_do = {
			factor = 1
		}
	}
}