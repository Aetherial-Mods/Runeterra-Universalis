country_decisions = {

	forge_gold = {
		potential = {
			always = yes
		}
		allow = {
			NOT = { inflation = 6 }
			NOT = { years_of_income = 3 }
			adm_power = 75
		}
		effect = {
			add_inflation = 2
			add_years_of_income = 1.0
			add_adm_power = -75
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	recruit_civilians_to_army = {
		potential = {
			always = yes
		}
		allow = {
			NOT = { manpower_percentage = 0.9 }
			prestige = 10
			mil_power = 75
		}
		effect = {
			add_yearly_manpower	= 2.5
			add_prestige = -10
			add_mil_power = -75
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	recruit_civilians_to_fleet = {
		potential = {
			any_owned_province = {
				has_port = yes
			}
		}
		allow = {
			NOT = { sailors_percentage = 0.9 }
			prestige = 10
			dip_power = 75
		}
		effect = {
			add_yearly_sailors	= 2.5
			add_prestige = -10
			add_dip_power = -75
		}
		ai_will_do = {
			factor = 0
		}
	}
}