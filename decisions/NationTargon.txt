country_decisions = {

	targon_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_targon_flag }
			NOT = { tag = TAR }
			NOT = { tag = SHU }
			NOT = { tag = GSH }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			religion_group = ascended_group
			capital_scope = {
				OR = {
					region = kalamanda_region
					region = old_shurima_region
					region = marai_coast_region
					region = targon_region
				}
			}
		}
		provinces_to_highlight = {
			province_id = 1333
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = TAR }
			owns_core_province = 1333
			capital_scope = {
				province_id = 1333
			}
		}
		effect = {
			change_tag = TAR
			swap_non_generic_missions = yes
			if = {
				limit = {
					NOT = { has_idea_group = TAR_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_targon_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}