# jaa_slot_1 = {
# 	slot = 1
# 	generic = yes
# 	ai = yes
# 	potential = {
# 		capital_scope = {
# 		superregion = shurima_superregion
# 		}
# 	}
# 	has_country_shield = yes
# 	jaa_push_northwest = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 3
# 		required_missions = { jaa_expand_ports }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_establish_western_defenses = {
# 		icon = mission_rb_unite_the_clans
# 		position = 4 
# 		required_missions = { jaa_push_northwest }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_sabatoge_nexarian_supplies = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 8
# 		required_missions = { jaa_prepare_attack_north }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# }
# jaa_slot_2 = {
# 	slot = 2
# 	generic = yes
# 	ai = yes
# 	potential = {
# 		capital_scope = {
# 		superregion = shurima_superregion
# 		}
# 	}
# 	has_country_shield = yes
# 	jaa_alliances = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 1
# 		required_missions = { }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_develop_the_mountains = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 4
# 		required_missions = { jaa_push_northwest }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_exploit_the_regional_wildlife = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 5
# 		required_missions = { jaa_develop_the_mountains }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_prepare_attack_north = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 7
# 		required_missions = { jaa_look_north_look_south }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_conquest_central_noxus = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 8
# 		required_missions = { jaa_prepare_attack_north }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_integrate_the_hinterlands = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 9
# 		required_missions = { jaa_conquest_central_noxus }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
	
# 	jaa_noxus_formation = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 10
# 		required_missions = { jaa_integrate_the_hinterlands}
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# }
# jaa_slot_3 = {
# 	slot = 3
# 	generic = yes
# 	ai = yes
# 	potential = {
# 		capital_scope = {
# 		superregion = shurima_superregion
# 		}
# 	}
# 	has_country_shield = yes
# 	jaa_expand_ports = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 2
# 		required_missions = { jaa_alliances jaa_trade_expansion }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_trade_dominance = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 4
# 		required_missions = { jaa_push_northwest jaa_push_east}
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_look_north_look_south = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 6
# 		required_missions = { jaa_exploit_the_regional_wildlife jaa_trade_dominance jaa_foster_jandaran_culture }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# }
# jaa_slot_4 = {
# 	slot = 4
# 	generic = yes
# 	ai = yes
# 	potential = {
# 		capital_scope = {
# 		superregion = shurima_superregion
# 		}
# 	}
# 	has_country_shield = yes
# 	jaa_trade_expansion = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 1
# 		required_missions = { }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_develop_the_port_cities = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 4
# 		required_missions = { jaa_push_east }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_foster_jandaran_culture = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 5
# 		required_missions = { jaa_develop_the_port_cities }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_guardian_of_cliff = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 7
# 		required_missions = { jaa_look_north_look_south }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_provoke_shuriman_infighting = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 8
# 		required_missions = { jaa_guardian_of_cliff }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_monopolize_the_channel = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 9

# 		required_missions = { jaa_provoke_shuriman_infighting }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_great_jandera = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 10
# 		required_missions = { jaa_monopolize_the_channel }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# }
# jaa_slot_5 = {
# 	slot = 5
# 	generic = yes
# 	ai = yes
# 	potential = {
# 		capital_scope = {
# 		superregion = shurima_superregion
# 		}
# 	}
# 	has_country_shield = yes
# 	jaa_push_east = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 3
# 		required_missions = { jaa_expand_ports }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_establish_eastern_defenses = {
# 		icon = mission_rb_unite_the_clans
# 		position = 4 
# 		required_missions = { jaa_push_east }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# 	jaa_develop_the_cliff = {
# 		icon = mission_rb_unite_the_clans 
# 		position = 8
# 		required_missions = { jaa_guardian_of_cliff }
# 		provinces_to_highlight = {
			
# 		}
# 		trigger = {
			
# 		}
# 		effect = {
			
# 		}
# 	}
# }