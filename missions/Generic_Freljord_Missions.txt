generic_freljord_slot_1 = {
	slot = 1
	generic = yes
	ai = yes
	potential = {
		capital_scope = {
		superregion = freljord_superregion
		}
	}
	has_country_shield = no
}
generic_freljord_slot_2 = {
	slot = 2
	generic = yes
	ai = yes
	potential = {
		capital_scope = {
		superregion = freljord_superregion
		}
	}
	has_country_shield = no
	generic_freljord_expand = {
		icon = mission_rb_conquer_brittany
		position = 2
		required_missions = { generic_freljord_stabilize_the_realm }
		provinces_to_highlight = {
			
		}
		trigger = {
			OR = {
			grown_by_development = 20
			grown_by_states = 3
			}
		}
		effect = {
			add_country_modifier = {
				name = "expansionist_country"
				duration = 9125 #25 years
			}
		}
	}
	generic_freljord_utilize_local_fauna = {
		icon = mission_pol_breadbasket_europe
		position = 4
		required_missions = { generic_freljord_expand }
		provinces_to_highlight = {
			
		}
		trigger = {
			adm_power_cost = 75
			OR = {	
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = avarosa_region
					value = 3
				}
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = east_freljord_region
					value = 3
				}
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = frostguards_region
					value = 3
				}
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = east_ice_sea_coast_region
					value = 3
				}
                num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = west_ice_sea_coast_region
					value = 3
				}
                num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					region = west_freljord_region
					value = 3
				}
			}
		}
		effect = {
			custom_tooltip = generic_utilize_local_fauna_tooltip
			if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
							region = avarosa_region
							value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_freljordian_grain_domestication"
					duration = -1
				}
			}
			if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
						region = east_freljord_region
						value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_elnuk_domestication"
					duration = -1
				}
			}
			if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
						region = frostguards_region
						value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_mammoth_domestication"
					duration = -1
				}
			}
			if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
						region = east_ice_sea_coast_region
						value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_druvask_domestication"
					duration = -1
				}
			}
            if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
						region = west_ice_sea_coast_region
						value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_grellfeen_domestication"
					duration = -1
				}
			}
            if = {
				limit = {
					num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
						region = west_freljord_region
						value = 3
					}
				}
				add_country_modifier = {
					name = "freljord_raptor_domestication"
					duration = -1
				}
			}
		}
	}

	generic_freljord_fortification_efforts = {
		icon = mission_control_the_desh 
		position = 5
		required_missions = { generic_freljord_utilize_local_fauna generic_freljord_ensure_faction_loyalty }
		provinces_to_highlight = {
			
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 2
				fort_level = 1
			}
		}
		effect = {
			add_country_modifier = {
				name = "defensive_focus"
				duration = 5475 #15 years
			}
		}
	}
	
	generic_freljord_culture_synergization = {
		icon = friends_in_high_places
		position = 7
		required_missions = { generic_freljord_curtail_hinterlands_autonomy }
		provinces_to_highlight = {
			
		}
		trigger = {
			dip_power_cost = 100
            prestige = 20
		}
		effect = {
			change_innovativeness = 5
			add_country_modifier = {
				name = "cultural_integration"
				duration = 5475 #15 years
			}
		}
	}
	generic_freljord_develop_the_homeland = {
		icon = mission_pol_develop_countryside
		position = 8
		required_missions = { generic_freljord_culture_synergization }
		provinces_to_highlight = {
			
		}
		trigger = {
            AND = {
				crown_land_share = 30
            	development_in_provinces = {
                	area_for_scope_province = {
                	is_capital = yes
                	}
                	value = 70
            	}
				num_of_owned_provinces_with = {
					is_prosperous = yes
					value = 5
				}
   		    }
		}
	effect = {
			add_country_modifier = {
				name = "prospourous_bliss"
				duration = 18250
			}
			add_adm_power = 150
		}
	}
}
generic_freljord_slot_3 = {
	slot = 3
	generic = yes
	ai = yes
	potential = {
		capital_scope = {
		superregion = freljord_superregion
		}
	}
	has_country_shield = no
	generic_freljord_stabilize_the_realm = {
		icon = balance_the_estates
		position = 1
		required_missions = {  }
		provinces_to_highlight = {
			
		}
		trigger = {
			AND = {
				manpower_percentage = 0.75
				OR = {
				num_of_royal_marriages = 1
				stability = 1
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = "tempered_populace"
				duration = 1825
			}
		}
	}
	generic_freljord_build_military_buildings = {
		icon = mission_castle_construction
		position = 3
		required_missions = {  }
		provinces_to_highlight = {
			
		}
		trigger = {
			AND = {
				ru_traininggrounds = 1
				ru_barracks = 1
			}
		}
		effect = {
			add_country_modifier = {
				name = "the_regimental_system"
				duration = 3650 #10 years
			}
		}
	}
	generic_freljord_ensure_faction_loyalty = {
		icon = mission_unite_the_princes
		position = 4
		required_missions = { generic_freljord_build_military_buildings }
		provinces_to_highlight = {
			
		}
		trigger = {
			if = {
				limit = { ai = no }
				AND = {
				estate_loyalty = {
				estate = estate_burghers
				loyalty = 50
				}
				estate_loyalty = {
				estate = estate_church
				loyalty = 50
				}
				estate_loyalty = {
				estate = estate_nobles
				loyalty = 50
				}
				}
			}
			else = {
				AND = {
					estate_loyalty = {
					estate = estate_burghers
					loyalty = 30
					}
					estate_loyalty = {
					estate = estate_church
					loyalty = 30
					}
					estate_loyalty = {
					estate = estate_nobles
					loyalty = 30
					}
				}	
			}
		}
		effect = {
			change_dip = 1
			change_heir_dip = 1
		}
	}
	generic_freljord_appease_the_cabal = {
		icon = mission_noble_council
		position = 5
		required_missions = { generic_freljord_ensure_faction_loyalty }
		provinces_to_highlight = {
			if = {
				religion = frostguards
				}
            religion = freljordian_pantheon
            }
			else = {
				
            }
		trigger = {
			adm_power_cost = 75
			mil_power_cost = 75
			OR = {
				AND = {
					has_matching_religion = frostguards
					num_of_religion = {
						religion = freljordian_pantheon
						value = 3
					}
				}
				AND = {
					NOT = { has_matching_religion = frostguards }
					religious_unity = 0.9
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = "unscrupulous_connections"
				duration = 9125 # 25 years
			}
			add_country_modifier = {
				name = "cult_concessions"
				duration = 18250 # 50 years
			}
		}
	}
	generic_freljord_curtail_hinterlands_autonomy = {
		icon = mission_rb_unite_the_clans 
		position = 6
		required_missions = { generic_freljord_fortification_efforts generic_freljord_appease_the_cabal generic_freljord_trading_connections }
		provinces_to_highlight = {
			
		}
		trigger = {
			if = {
				limit = { ai = no }
				AND = {
				NOT = { average_autonomy_above_min = 20 }
				NOT = { average_unrest = 1 }
				}

			}
			else = {
				NOT = { average_autonomy_above_min = 40 }	
			}
		}
		effect = {
			add_country_modifier = {
				name = "hinterlands_restriction"
				duration = 9125
			}
		}
	}
	generic_freljord_meritocratic_gains = {
		icon = hungarian_union
		position = 8
		required_missions = { generic_freljord_culture_synergization generic_freljord_create_a_grand_fleet }
		provinces_to_highlight = {
			
		}
		trigger = {
			if = {
				limit = { ai = no }
				AND = {
				monthly_adm = 8
				monthly_dip = 8
				monthly_mil = 8
				}
			}
			else = {
				AND = {
				employed_advisor = { category = ADM }
				employed_advisor = { category = DIP }
				employed_advisor = { category = MIL }
				}	
			}
		}
		effect = {
			camavor = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			great_strait = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { is_permanent_claim = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "imperial_ambition"
				duration = 9125
			}
		}
	}
	generic_freljord_grand_freljord = {
		icon = secure_the_republic
		position = 9
		required_missions = { generic_freljord_meritocratic_gains }
		provinces_to_highlight = {
			province_id = 82
		}
		trigger = {
			owns_core_province = 140		# Frostguard Citadel
			owns_core_province = 1070		# Vathcaer
			owns_core_province = 523		# Palpaa
			owns_core_province = 969		# Rakelstake
	        969 = {							# Rakelstake
				is_state = yes
			}
		}
		effect = {
			add_adm_power = 100
			add_prestige = 30
		}
	}
}
generic_freljord_slot_4 = {
	slot = 4
	generic = yes
	ai = yes
	potential = {
		capital_scope = {
		superregion = freljord_superregion
		}
	}
	has_country_shield = no
	generic_freljord_expand_the_diplomatic_retinue = {
		icon = strengthen_the_vroedschappen
		position = 2
		required_missions = { generic_freljord_stabilize_the_realm }
		provinces_to_highlight = {
			
		}
		trigger = {
			AND = {
				diplomatic_reputation = 1
				dip_power_cost = 100
				calc_true_if = {
					all_ally = {
						has_opinion = {
							who = ROOT
							value = 150
						}
					}
					amount = 1
				}
			}

		}
		effect = {
			add_country_modifier = {
				name = "diplomatic_connections"
				duration = 9125
			}
		}
	}
	generic_freljord_exploit_the_land = {
		icon = mission_found_arkhangelsk
		position = 4
		required_missions = { generic_freljord_expand_the_diplomatic_retinue }
		provinces_to_highlight = {
			
		}
		trigger = {
			ru_mine = 3
		}
		effect = {
			add_country_modifier = {
				name = "strike_the_earth"
				duration = 9125
			}
		}
	}
	generic_freljord_trading_connections = {
		icon = mission_steer_trade_to_bengal
		position = 5
		required_missions = { generic_freljord_ensure_faction_loyalty generic_freljord_exploit_the_land }
		provinces_to_highlight = {
			
		}
		trigger = {
			if = {
				limit = { ai = no }
				1987 = { #Camavor
					trade_share = {
						country = ROOT
						share = 75
					}
				}
				281 = { #Great Strait
					trade_share = {
						country = ROOT
						share = 25
					}
				}
			}
			else = {
				1987 = { #Camavor
					trade_share = {
						country = ROOT
						share = 40
					}
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = "growing_economy"
				duration = 9125 #25 years
			}
		}
	}
	generic_freljord_create_a_grand_fleet = {
		icon = mission_grand_bengali_shipyards
		position = 7
		required_missions = { generic_freljord_curtail_hinterlands_autonomy }
		provinces_to_highlight = {
			
		}
		trigger = {
			if = {
				limit = { ai = no }
				navy_size = 25
				navy_size_percentage = 1
				num_of_admirals = 1
			}
			else = {
				navy_size = 20
			}
		}
		effect = {
			add_navy_tradition = 10	
			create_admiral = { tradition = 70 }	
		}
	}
	generic_freljord_further_develop_trade = {
		icon = mission_rb_colonise_spice_islands
		position = 8
		required_missions = { generic_freljord_create_a_grand_fleet }
		provinces_to_highlight = {
			
		}
		trigger = {
			mercantilism = 5
			num_of_centers_of_trade	= 5
		}
		effect = {
			add_mercantilism = 5
			change_government_reform_progress = 50
		}
	}
}
generic_freljord_slot_5 = {
	slot = 5
	generic = yes
	ai = yes
	potential = {
		capital_scope = {
		superregion = freljord_superregion
		}
	}
	has_country_shield = no
}