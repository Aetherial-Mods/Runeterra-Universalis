add_core = JGG
owner = JGG
controller = JGG
religion = freljordian_pantheon
culture = stormborn
trade_goods = fur
capital = "Placeholder"
base_tax = 3
base_production = 3
base_manpower = 3
discovered_by = freljordian
is_city = yes
