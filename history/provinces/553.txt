add_core = DEL
owner = DEL
controller = DEL
religion = the_winged_protector
culture = meridianus
trade_goods = petricit
capital = "Placeholder"
base_tax = 3
base_production = 3
base_manpower = 3
discovered_by = demacian
is_city = yes
hre = yes