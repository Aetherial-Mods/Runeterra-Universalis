native_size = 5
native_ferocity = 5
native_hostileness = 10
religion = lamb_and_wolf
culture = efkarani
trade_goods = unknown
capital = "Placeholder"
base_tax = 1
base_production = 1
base_manpower = 1
discovered_by = noxian
discovered_by = demacian