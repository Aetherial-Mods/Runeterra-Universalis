add_core = SHU
owner = SHU
controller = SHU
religion = eternals
culture = ascended
trade_goods = god_essence
capital = "The Sun Disc"
base_tax = 7
base_production = 7
base_manpower = 7
center_of_trade = 2
is_city = yes

ru_fort_1 = yes

discovered_by = shuriman
discovered_by = void