add_core = TRK
owner = TRK
controller = TRK
religion = freljordian_pantheon
culture = wanderers
trade_goods = ivory
capital = "Placeholder"
base_tax = 1
base_production = 1
base_manpower = 1
discovered_by = freljordian
is_city = yes
