add_core = TRF
owner = TRF
controller = TRF
religion = freljordian_pantheon
culture = troll
trade_goods = iron
capital = "Placeholder"
base_tax = 3
base_production = 3
base_manpower = 3
discovered_by = freljordian
is_city = yes
