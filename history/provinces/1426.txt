add_core = CAM
owner = CAM
controller = CAM
religion = the_hallowed_mist
culture = camavorian
trade_goods = fish
capital = "Placeholder"
base_tax = 2
base_production = 2
base_manpower = 2
# Anything typed here will be added to all selected province history files
discovered_by = camavorian
is_city = yes
