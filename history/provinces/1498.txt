add_core = AUE
owner = AUE
controller = AUE
religion = the_winged_protector
culture = sylvane
trade_goods = iron
capital = "Placeholder"
base_tax = 3
base_production = 3
base_manpower = 3
discovered_by = demacian
is_city = yes
hre = yes