add_core = SOT
owner = SOT
controller = SOT
religion = lamb_and_wolf
culture = nyani
trade_goods = wine
capital = "Placeholder"
base_tax = 2
base_production = 2
base_manpower = 2
discovered_by = noxian
discovered_by = demacian
discovered_by = zaunite
is_city = yes