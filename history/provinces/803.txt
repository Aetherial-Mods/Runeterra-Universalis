add_core = XEL
owner = XEL
controller = XEL
religion = lamb_and_wolf
culture = lorengarde
trade_goods = livestock
capital = "Placeholder"
base_tax = 2
base_production = 2
base_manpower = 2
discovered_by = freljordian
discovered_by = demacian
is_city = yes