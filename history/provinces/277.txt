add_core = LIH
owner = LIH
controller = LIH
religion = freljordian_pantheon
culture = barbarian
trade_goods = wool
capital = "Placeholder"
base_tax = 3
base_production = 3
base_manpower = 3
discovered_by = freljordian
is_city = yes
