native_size = 5
native_ferocity = 5
native_hostileness = 10
religion = great_plains
culture = desert_travellers
trade_goods = unknown
capital = "Placeholder"
base_tax = 1
base_production = 1
base_manpower = 1
# Anything typed here will be added to all selected province history files
discovered_by = camavorian