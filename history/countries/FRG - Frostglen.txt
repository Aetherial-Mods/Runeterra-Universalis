government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = avarosan
capital = 1045

300.1.1 = {
	monarch = {
		name = "Asmond"
		dynasty = "Frieda"
		adm = 6
		dip = 5
		mil = 1
		birth_date = 282.1.1
		death_date = 342.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Gunnar"
		dynasty = "Frieda"
		adm = 3
		dip = 4
		mil = 2
		birth_date = 297.1.1
		death_date = 357.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}