government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = sylvane
capital = 847

300.1.1 = {
	monarch = {
		name = "Hector"
		dynasty = "Stirling"
		adm = 4
		dip = 6
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = slyvane
	}
	heir = {
		name = "Maelgwyn"
		dynasty = "Stirling"
		adm = 4
		dip = 3
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = slyvane
		claim = 100
	}
}