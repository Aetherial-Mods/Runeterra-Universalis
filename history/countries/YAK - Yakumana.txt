government = monarchy
government_rank = 1
mercantilism = 0
technology_group = zaunite
religion = the_glorious_evolved
primary_culture = zaunite
capital = 1414

300.1.1 = {
	monarch = {
		name = "Gallius"
		dynasty = "Evangellia"
		adm = 4
		dip = 5
		mil = 2
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = the_glorious_evolved
		culture = zaunite
	}
	heir = {
		name = "Sandbowl"
		dynasty = "Evangellia"
		adm = 5
		dip = 1
		mil = 4
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = the_glorious_evolved
		culture = zaunite
		claim = 100
	}
}