government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = veilstead
capital = 357

300.1.1 = {
	monarch = {
		name = "Thaddeus"
		dynasty = "Dawnblade"
		adm = 3
		dip = 4
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = veilstead
	}
	heir = {
		name = "Uther"
		dynasty = "Dawnblade"
		adm = 2
		dip = 4
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = veilstead
		claim = 100
	}
}