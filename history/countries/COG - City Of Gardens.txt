government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1261

300.1.1 = {
	monarch = {
		name = "Artavez"
		dynasty = "Khashaba"
		adm = 7
		dip = 4
		mil = 0
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Karmike"
		dynasty = "Khashaba"
		adm = 4
		dip = 4
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}