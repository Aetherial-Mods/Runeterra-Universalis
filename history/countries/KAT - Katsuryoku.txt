government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = nature_school
primary_culture = wuju
capital = 186

300.1.1 = {
	monarch = {
		name = "Minusan"
		dynasty = "Wuju"
		adm = 1
		dip = 0
		mil = 3
		birth_date = 272.11.2
		death_date = 332.10.14
		religion = spirit_blossom
		culture = wuju
	}
	heir = {
		name = "Mirisan"
		dynasty = "Wuju"
		adm = 2
		dip = 2
		mil = 5
		birth_date = 287.8.4
		death_date = 347.5.15
		religion = spirit_blossom
		culture = wuju
		claim = 100
	}
}