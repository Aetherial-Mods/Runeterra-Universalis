government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = lamb_and_wolf
primary_culture = marai
capital = 400

300.1.1 = {
	monarch = {
		name = "Tistoe"
		dynasty = "Barbae"
		adm = 1
		dip = 1
		mil = 1
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = marai
	}
}