government = native
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = axiomata
religious_school = water_school
primary_culture = kumungian
capital = 1304

300.1.1 = {
	monarch = {
		name = "Lanu"
		adm = 5
		dip = 0
		mil = 5
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = axiomata
		culture = kumungian
	}
}