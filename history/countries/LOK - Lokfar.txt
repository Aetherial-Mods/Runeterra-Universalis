government = native
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = wanderers
capital = 1323

300.1.1 = {
	monarch = {
		name = "Loksam"
		dynasty = "Asim"
		adm = 3
		dip = 6
		mil = 5
		birth_date = 284.1.1
		death_date = 344.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Najak"
		dynasty = "Asim"
		adm = 5
		dip = 5
		mil = 5
		birth_date = 299.1.1
		death_date = 359.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}