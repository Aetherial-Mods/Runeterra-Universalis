government = hivemind
government_rank = 1
mercantilism = 0
technology_group = void
religion = the_watchers
primary_culture = watcher
capital = 1342

300.1.1 = {
	monarch = {
		name = "Kar'Mik"
		adm = 5
		dip = 0
		mil = 0
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = the_watchers
		culture = watcher
	}
}