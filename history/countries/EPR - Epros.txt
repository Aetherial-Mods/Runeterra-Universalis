government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = tiberian
capital = 394

300.1.1 = {
	monarch = {
		name = "Morvran"
		dynasty = "Stormhammer"
		adm = 2
		dip = 5
		mil = 2
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = tiberian
	}
	heir = {
		name = "Elsinore"
		dynasty = "Stormhammer"
		adm = 3
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = tiberian
		claim = 100
	}
}