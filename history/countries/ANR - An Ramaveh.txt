government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = marai
capital = 1256

300.1.1 = {
	monarch = {
		name = "Seker"
		dynasty = "Khashaba"
		adm = 2
		dip = 0
		mil = 3
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = marai
	}
}