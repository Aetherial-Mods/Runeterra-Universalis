government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 917

300.1.1 = {
	monarch = {
		name = "Menkhe"
		dynasty = "Zagayah"
		adm = 5
		dip = 4
		mil = 2
		birth_date = 272.1.1
		death_date = 332.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Ytuli"
		dynasty = "Zagayah"
		adm = 1
		dip = 6
		mil = 4
		birth_date = 287.1.1
		death_date = 347.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}