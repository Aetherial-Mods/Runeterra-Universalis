government = tribal
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = axiomata
religious_school = magma_school
primary_culture = ixaocania
capital = 1612

300.1.1 = {
	monarch = {
		name = "Icnoyotl"
		dynasty = "Ixtlilin"
		adm = 2
		dip = 3
		mil = 1
		birth_date = 282.1.1
		death_date = 342.1.1
		religion = axiomata
		culture = ixaocania
	}
	
	heir = {
		name = "Sachual"
		dynasty = "Ixtlilin"
		adm = 1
		dip = 5
		mil = 3
		birth_date = 298.1.1
		death_date = 358.1.1
		religion = axiomata
		culture = ixaocania
		claim = 100
	}
}