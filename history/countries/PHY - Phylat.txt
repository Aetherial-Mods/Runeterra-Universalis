government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = water_school
primary_culture = navorian
capital = 284

300.1.1 = {
	monarch = {
		name = "Etora"
		dynasty = "Ko"
		adm = 0
		dip = 6
		mil = 3
		birth_date = 249.7.21
		death_date = 309.2.13
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Nodora"
		dynasty = "Ko"
		adm = 0
		dip = 2
		mil = 6
		birth_date = 264.2.24
		death_date = 324.12.8
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}