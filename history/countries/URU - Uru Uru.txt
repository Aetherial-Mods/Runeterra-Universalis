government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = water_school
primary_culture = navorian
capital = 1481

300.1.1 = {
	monarch = {
		name = "Baiduru"
		dynasty = "Uru"
		adm = 1
		dip = 4
		mil = 1
		birth_date = 283.11.17
		death_date = 343.7.2
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Penpuru"
		dynasty = "Uru"
		adm = 2
		dip = 4
		mil = 0
		birth_date = 298.9.20
		death_date = 358.9.5
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}