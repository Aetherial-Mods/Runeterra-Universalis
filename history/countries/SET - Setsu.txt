government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = magma_school
primary_culture = galrin
capital = 218

300.1.1 = {
	monarch = {
		name = "Mae"
		dynasty = "Setsaru"
		adm = 2
		dip = 3
		mil = 0
		birth_date = 279.8.4
		death_date = 339.5.9
		religion = spirit_blossom
		culture = galrin
	}
	heir = {
		name = "Stuo"
		dynasty = "Setsaru"
		adm = 5
		dip = 2
		mil = 1
		birth_date = 294.3.1
		death_date = 354.6.24
		religion = spirit_blossom
		culture = galrin
		claim = 100
	}
}