government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1562

300.1.1 = {
	monarch = {
		name = "Resephi"
		dynasty = "Barbae"
		adm = 1
		dip = 6
		mil = 5
		birth_date = 278.1.1
		death_date = 338.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Psischod"
		dynasty = "Barbae"
		adm = 3
		dip = 5
		mil = 3
		birth_date = 293.1.1
		death_date = 353.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}