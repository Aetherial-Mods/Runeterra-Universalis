government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1390

300.1.1 = {
	monarch = {
		name = "Horok"
		dynasty = "Khashaba"
		adm = 3
		dip = 2
		mil = 2
		birth_date = 271.1.1
		death_date = 331.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Ytuli"
		dynasty = "Khashaba"
		adm = 4
		dip = 2
		mil = 4
		birth_date = 286.1.1
		death_date = 346.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}