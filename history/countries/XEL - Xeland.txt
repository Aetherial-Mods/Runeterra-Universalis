government = monarchy
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = lamb_and_wolf
primary_culture = lorengarde
capital = 1516

300.1.1 = {
	monarch = {
		name = "Iyeiyea"
		dynasty = "Star"
		adm = 3
		dip = 6
		mil = 3
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = lamb_and_wolf
		culture = lorengarde
	}
	heir = {
		name = "Shievo"
		dynasty = "Star"
		adm = 5
		dip = 4
		mil = 1
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = lamb_and_wolf
		culture = lorengarde
		claim = 100
	}
}