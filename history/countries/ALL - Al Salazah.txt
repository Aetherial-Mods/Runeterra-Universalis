government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = marai
capital = 1301

300.1.1 = {
	monarch = {
		name = "Funsani"
		dynasty = "Khashaba"
		adm = 3
		dip = 1
		mil = 1
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = marai
	}
}