government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1706

300.1.1 = {
	monarch = {
		name = "Khaldun"
		dynasty = "Barbae"
		adm = 1
		dip = 3
		mil = 2
		birth_date = 274.1.1
		death_date = 334.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Malouf"
		dynasty = "Barbae"
		adm = 4
		dip = 1
		mil = 5
		birth_date = 290.1.1
		death_date = 350.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}