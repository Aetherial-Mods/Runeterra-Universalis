government = monarchy
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = mother_serpent
primary_culture = buhru
capital = 1674

300.1.1 = {
	monarch = {
		name = "Pimne"
		dynasty = "Huata"
		adm = 1
		dip = 2
		mil = 3
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = mother_serpent
		culture = buhru
	}
	heir = {
		name = "Tayanita"
		dynasty = "Huata"
		adm = 2
		dip = 3
		mil = 4
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = mother_serpent
		culture = buhru
		claim = 100
	}
}