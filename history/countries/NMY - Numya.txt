government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = light_school
primary_culture = kinkou
capital = 1188

300.1.1 = {
	monarch = {
		name = "Nunya"
		dynasty = "Tashetashepounguo"
		adm = 1
		dip = 1
		mil = 2
		birth_date = 272.1.1
		death_date = 332.1.1
		religion = spirit_blossom
		culture = kinkou
	}
	heir = {
		name = "Shigoto"
		dynasty = "Tashetashepounguo"
		adm = 4
		dip = 4
		mil = 2
		birth_date = 287.1.1
		death_date = 347.1.1
		religion = spirit_blossom
		culture = kinkou
		claim = 100
	}
}