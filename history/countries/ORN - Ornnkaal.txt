government = theocracy
add_government_reform = godhood_reform
government_rank = 2
mercantilism = 10
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = barbarian
capital = 893

historical_rival = URS

historical_friend = ORN

260.1.1 = {
	monarch = {
		name = "Ornn"
		dynasty = "God"
		adm = 7
		dip = 2
		mil = 2
		birth_date = 260.1.1
		religion = freljordian_pantheon
		culture = barbarian
	}
	
	set_country_flag = is_ornn_flag
}

300.1.1 = {
	add_truce_with = QUC
}