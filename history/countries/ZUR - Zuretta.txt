government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = nashramaen
capital = 921

300.1.1 = {
	monarch = {
		name = "Rhoksha"
		dynasty = "Kolistera"
		adm = 2
		dip = 5
		mil = 1
		birth_date = 268.1.1
		death_date = 328.1.1
		religion = eternals
		culture = nashramaen
	}
	heir = {
		name = "Zakhiro"
		dynasty = "Kolistera"
		adm = 1
		dip = 3
		mil = 2
		birth_date = 283.1.1
		death_date = 343.1.1
		religion = eternals
		culture = nashramaen
		claim = 100
	}
}