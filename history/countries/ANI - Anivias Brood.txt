government = theocracy
add_government_reform = godhood_reform
government_rank = 2
mercantilism = 10
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = frostguards
capital = 1407

historical_friend = ORN

300.1.1 = {
	monarch = {
		name = "Anivia"
		dynasty = "God"
		adm = 2
		dip = 7
		mil = 2
		birth_date = 260.1.1
		female = yes
		religion = freljordian_pantheon
		culture = frostguards
	}
	
	set_country_flag = is_anivia_flag
}