government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = water_school
primary_culture = navorian
capital = 1307

300.1.1 = {
	monarch = {
		name = "Saito"
		dynasty = "Shig"
		adm = 1
		dip = 4
		mil = 3
		birth_date = 284.1.1
		death_date = 344.1.1
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Gima"
		dynasty = "Shig"
		adm = 3
		dip = 3
		mil = 1
		birth_date = 299.1.1
		death_date = 359.1.1
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}