government = monarchy
government_rank = 1
mercantilism = 0
technology_group = zaunite
religion = the_glorious_evolved
primary_culture = zaunite
capital = 963

300.1.1 = {
	monarch = {
		name = "Lucian"
		dynasty = "Maxamillian"
		adm = 3
		dip = 2
		mil = 4
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = the_glorious_evolved
		culture = zaunite
	}
	heir = {
		name = "Ophello"
		dynasty = "Maxamillian"
		adm = 5
		dip = 4
		mil = 1
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = the_glorious_evolved
		culture = zaunite
		claim = 100
	}
}