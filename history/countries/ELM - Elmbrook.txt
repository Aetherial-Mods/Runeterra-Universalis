government = republic
add_government_reform = free_city
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = tiberian
capital = 80

300.1.1 = {
	monarch = {
		name = "Kaldrin"
		dynasty = "Starborne"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = tiberian
	}
	heir = {
		name = "Lorianus"
		dynasty = "Starborne"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = tiberian
		claim = 100
	}
}