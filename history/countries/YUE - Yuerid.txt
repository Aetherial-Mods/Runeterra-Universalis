government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1515

300.1.1 = {
	monarch = {
		name = "Khaldun"
		dynasty = "Barbae"
		adm = 4
		dip = 6
		mil = 2
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Palet"
		dynasty = "Barbae"
		adm = 4
		dip = 3
		mil = 3
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}