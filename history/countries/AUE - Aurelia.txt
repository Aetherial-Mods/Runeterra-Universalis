government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = sylvane
capital = 1039

300.1.1 = {
	monarch = {
		name = "Uvander"
		dynasty = "Merrick"
		adm = 3
		dip = 4
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = slyvane
	}
	heir = {
		name = "Thorne"
		dynasty = "Merrick"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = slyvane
		claim = 100
	}
}