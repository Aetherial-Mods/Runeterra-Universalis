government = native
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = wanderers
capital = 183

300.1.1 = {
	monarch = {
		name = "Steen"
		dynasty = "Asim"
		adm = 5
		dip = 4
		mil = 6
		birth_date = 275.1.1
		death_date = 335.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Knul"
		dynasty = "Asim"
		adm = 1
		dip = 6
		mil = 3
		birth_date = 290.1.1
		death_date = 350.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}