government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = troll
capital = 1159

historical_rival = TRI
historical_rival = TRC
historical_rival = TRF
historical_rival = TCL
historical_rival = TRH
historical_rival = TRR

300.1.1 = {
	monarch = {
		name = "Erodjan"
		dynasty = "Troll"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = freljordian_pantheon
		culture = troll
	}
	heir = {
		name = "Zulkis"
		dynasty = "Troll"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = freljordian_pantheon
		culture = troll
		claim = 100
	}
}