government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = kalamanda
capital = 980

300.1.1 = {
	monarch = {
		name = "Jabbo"
		dynasty = "Shira"
		adm = 1
		dip = 1
		mil = 4
		birth_date = 272.1.1
		death_date = 332.1.1
		religion = eternals
		culture = kalamanda
	}
	heir = {
		name = "Resephi"
		dynasty = "Shira"
		adm = 6
		dip = 5
		mil = 0
		birth_date = 287.1.1
		death_date = 347.1.1
		religion = eternals
		culture = kalamanda
		claim = 100
	}
}