government = monarchy
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = nature_school
primary_culture = wuju
capital = 672

300.1.1 = {
	monarch = {
		name = "Kobayashi"
		dynasty = "Wuju"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 275.1.1
		death_date = 335.1.1
		religion = spirit_blossom
		culture = wuju
	}
	heir = {
		name = "Homura"
		dynasty = "Wuju"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 290.1.1
		death_date = 350.1.1
		religion = spirit_blossom
		culture = wuju
		claim = 100
	}
}