government = tribal
government_rank = 1
mercantilism = 0
technology_group = camavorian
religion = great_plains
primary_culture = desert_travellers
capital = 1545

300.1.1 = {
	monarch = {
		name = "Soar"
		dynasty = "Contour"
		adm = 3
		dip = 5
		mil = 1
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = great_plains
		culture = desert_travellers
	}
}