government = monarchy
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = avarosan
capital = 160

300.1.1 = {
	monarch = {
		name = "Rad"
		dynasty = "Demigod"
		adm = 1
		dip = 4
		mil = 2
		birth_date = 277.1.1
		death_date = 337.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Svatveti"
		dynasty = "Demigod"
		adm = 6
		dip = 3
		mil = 1
		birth_date = 293.1.1
		death_date = 353.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}