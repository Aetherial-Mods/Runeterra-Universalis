government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = troll
capital = 777

historical_rival = TRI
historical_rival = TRB
historical_rival = TRC
historical_rival = TCL
historical_rival = TRH
historical_rival = TRR

300.1.1 = {
	monarch = {
		name = "Ugoki"
		dynasty = "Troll"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = freljordian_pantheon
		culture = troll
	}
	heir = {
		name = "Paikei"
		dynasty = "Troll"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = freljordian_pantheon
		culture = troll
		claim = 100
	}
}