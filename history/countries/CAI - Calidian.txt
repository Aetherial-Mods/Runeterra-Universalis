government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = ebonari
capital = 38

300.1.1 = {
	monarch = {
		name = "Tavish"
		dynasty = "Kensington"
		adm = 1
		dip = 4
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = ebonari
	}
	heir = {
		name = "Walden"
		dynasty = "Kensington"
		adm = 2
		dip = 1
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = ebonari
		claim = 100
	}
}