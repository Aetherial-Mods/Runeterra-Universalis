government = native
government_rank = 1
mercantilism = 0
technology_group = camavorian
religion = great_plains
primary_culture = desert_travellers
capital = 48

300.1.1 = {
	monarch = {
		name = "Cosane"
		dynasty = "Shirek"
		adm = 3
		dip = 2
		mil = 4
		birth_date = 284.1.1
		death_date = 344.1.1
		religion = great_plains
		culture = desert_travellers
	}
	heir = {
		name = "Ojr"
		dynasty = "Barnae"
		adm = 4
		dip = 2
		mil = 6
		birth_date = 299.1.1
		death_date = 359.1.1
		religion = great_plains
		culture = desert_travellers
		claim = 100
	}
}