government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = stormborn
capital = 873

300.1.1 = {
	monarch = {
		name = "Gunnar"
		dynasty = "Frieda"
		adm = 1
		dip = 3
		mil = 2
		birth_date = 268.1.1
		death_date = 328.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Derosa"
		dynasty = "Frieda"
		adm = 1
		dip = 2
		mil = 2
		birth_date = 283.1.1
		death_date = 343.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}