government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = aelarian
capital = 1668

300.1.1 = {
	monarch = {
		name = "Quindar"
		dynasty = "Forsythe"
		adm = 3
		dip = 6
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = aelarian
	}
	heir = {
		name = "Percivus"
		dynasty = "Forsythe"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = aelarian
		claim = 100
	}
}