government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = ascended
capital = 1020

300.1.1 = {
	monarch = {
		name = "Madpara"
		dynasty = "Setiu"
		adm = 2
		dip = 6
		mil = 3
		birth_date = 284.1.1
		death_date = 344.1.1
		religion = eternals
		culture = ascended
	}
	heir = {
		name = "Madpara II"
		dynasty = "Setiu"
		adm = 4
		dip = 1
		mil = 3
		birth_date = 299.1.1
		death_date = 359.1.1
		religion = eternals
		culture = ascended
		claim = 100
	}
}