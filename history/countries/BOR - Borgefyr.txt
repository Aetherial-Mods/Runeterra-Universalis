government = native
government_rank = 1
mercantilism = 0
technology_group = camavorian
religion = great_plains
primary_culture = desert_travellers
capital = 6

300.1.1 = {
	monarch = {
		name = "Wron"
		dynasty = "Feffinbridge"
		adm = 3
		dip = 4
		mil = 4
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = great_plains
		culture = desert_travellers
	}
	heir = {
		name = "Caldrid"
		dynasty = "Feffinbridge"
		adm = 2
		dip = 2
		mil = 3
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = great_plains
		culture = desert_travellers
		claim = 100
	}
}