government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = frostguards
primary_culture = stormborn
capital = 1488

300.1.1 = {
	monarch = {
		name = "Viljar"
		dynasty = "Brynhildr"
		adm = 2
		dip = 1
		mil = 2
		birth_date = 282.1.1
		death_date = 342.1.1
		religion = frostguards
		culture = avarosan
	}
	heir = {
		name = "Aalve"
		dynasty = "Brynhildr"
		adm = 6
		dip = 1
		mil = 4
		birth_date = 297.1.1
		death_date = 357.1.1
		religion = frostguards
		culture = avarosan
		claim = 100
	}
}