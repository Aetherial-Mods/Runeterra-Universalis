government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = kalamanda
capital = 141

300.1.1 = {
	monarch = {
		name = "Mekatsur"
		dynasty = "Yesheji"
		adm = 1
		dip = 2
		mil = 3
		birth_date = 265.1.1
		death_date = 325.1.1
		religion = eternals
		culture = kalamanda
	}
	heir = {
		name = "Poniuvos"
		dynasty = "Yesheji"
		adm = 5
		dip = 4
		mil = 3
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = eternals
		culture = kalamanda
		claim = 100
	}
}