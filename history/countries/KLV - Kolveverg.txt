government = tribal
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = frostguards
primary_culture = stormborn
capital = 1002

300.1.1 = {
	monarch = {
		name = "Trygve"
		dynasty = "Brynhildr"
		adm = 6
		dip = 1
		mil = 2
		birth_date = 278.1.1
		death_date = 338.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Kalkia"
		dynasty = "Brynhildr"
		adm = 3
		dip = 2
		mil = 3
		birth_date = 293.1.1
		death_date = 353.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}