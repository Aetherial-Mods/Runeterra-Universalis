government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = water_school
primary_culture = navorian
capital = 1698

300.1.1 = {
	monarch = {
		name = "Morioka"
		dynasty = "Saburo"
		adm = 4
		dip = 2
		mil = 4
		birth_date = 257.4.9
		death_date = 317.6.5
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Takai"
		dynasty = "Saburo"
		adm = 5
		dip = 6
		mil = 3
		birth_date = 272.5.13
		death_date = 332.9.25
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}