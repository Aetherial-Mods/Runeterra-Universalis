government = native
government_rank = 1
mercantilism = 0
technology_group = camavorian
religion = great_plains
primary_culture = desert_travellers
capital = 83

300.1.1 = {
	monarch = {
		name = "Ashan"
		dynasty = "Shani"
		adm = 3
		dip = 3
		mil = 4
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = great_plains
		culture = desert_travellers
	}
	heir = {
		name = "Ase"
		dynasty = "Shani"
		adm = 2
		dip = 3
		mil = 1
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = great_plains
		culture = desert_travellers
		claim = 100
	}
}