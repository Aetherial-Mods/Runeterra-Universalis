government = native
government_rank = 1
mercantilism = 0
technology_group = camavorian
religion = great_plains
primary_culture = desert_travellers
capital = 162

300.1.1 = {
	monarch = {
		name = "Date"
		dynasty = "Lokin"
		adm = 5
		dip = 4
		mil = 2
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = great_plains
		culture = desert_travellers
	}
	heir = {
		name = "Ignorin"
		dynasty = "Lokin"
		adm = 5
		dip = 3
		mil = 1
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = great_plains
		culture = desert_travellers
		claim = 100
	}
}