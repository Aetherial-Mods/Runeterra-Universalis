government = hivemind
government_rank = 1
mercantilism = 0
technology_group = void
religion = the_watchers
primary_culture = crawler
capital = 588

300.1.1 = {
	monarch = {
		name = "Sha'Tyx"
		adm = 0
		dip = 0
		mil = 5
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = the_watchers
		culture = crawler
	}
}