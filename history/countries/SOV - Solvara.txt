government = republic
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = sylvane
capital = 336

300.1.1 = {
	monarch = {
		name = "Caelum"
		dynasty = "Pendleton"
		adm = 1
		dip = 2
		mil = 4
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = sylvane
	}
	heir = {
		name = "Daelin"
		dynasty = "Stormweaver"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = sylvane
		claim = 100
	}
}