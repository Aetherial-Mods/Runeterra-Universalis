government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = meridianus
capital = 973

300.1.1 = {
	monarch = {
		name = "Quillen"
		dynasty = "Stormcloak"
		adm = 3
		dip = 4
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = meridianus
	}
	heir = {
		name = "Osrick"
		dynasty = "Stormcloak"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = meridianus
		claim = 100
	}
}