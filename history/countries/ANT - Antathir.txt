government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = ascended
capital = 287

300.1.1 = {
	monarch = {
		name = "Mejenes"
		dynasty = "Nemuku"
		adm = 5
		dip = 4
		mil = 5
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = ascended
	}
	heir = {
		name = "Yakub"
		dynasty = "Nemuku"
		adm = 6
		dip = 3
		mil = 4
		birth_date = 299.1.1
		death_date = 359.1.1
		religion = eternals
		culture = ascended
		claim = 100
	}
}