government = native
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = eternals
primary_culture = kumungian
capital = 548

300.1.1 = {
	monarch = {
		name = "Unaduti"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = kumungian
	}
}