government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = valerian
capital = 1709

300.1.1 = {
	monarch = {
		name = "Quinlan"
		dynasty = "Dawnblade"
		adm = 4
		dip = 3
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = valerian
	}
	heir = {
		name = "Roderic"
		dynasty = "Dawnblade"
		adm = 3
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = valerian
		claim = 100
	}
}