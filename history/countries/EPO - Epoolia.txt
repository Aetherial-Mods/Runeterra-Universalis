government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = nature_school
primary_culture = wuju
capital = 1238

300.1.1 = {
	monarch = {
		name = "Esolindia"
		dynasty = "Wuju"
		adm = 4
		dip = 5
		mil = 5
		birth_date = 263.4.1
		death_date = 323.4.17
		religion = spirit_blossom
		culture = wuju
	}
	heir = {
		name = "Bikarumina"
		dynasty = "Wuju"
		adm = 1
		dip = 4
		mil = 1
		birth_date = 278.6.27
		death_date = 338.5.14
		religion = spirit_blossom
		culture = wuju
		claim = 100
	}
}