government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = kalamanda
capital = 656

300.1.1 = {
	monarch = {
		name = "Satur"
		dynasty = "Yesheji"
		adm = 5
		dip = 5
		mil = 5
		birth_date = 276.1.1
		death_date = 336.1.1
		religion = eternals
		culture = kalamanda
	}
	heir = {
		name = "Majarzteh"
		dynasty = "Yesheji"
		adm = 3
		dip = 2
		mil = 1
		birth_date = 291.1.1
		death_date = 351.1.1
		religion = eternals
		culture = kalamanda
		claim = 100
	}
}