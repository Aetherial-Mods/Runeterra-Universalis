government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = ascended
capital = 1369

300.1.1 = {
	monarch = {
		name = "Lysimachus"
		dynasty = "Nemuku"
		adm = 4
		dip = 4
		mil = 2
		birth_date = 266.1.1
		death_date = 326.1.1
		religion = eternals
		culture = ascended
	}
	heir = {
		name = "Menkhe"
		dynasty = "Nemuku"
		adm = 6
		dip = 5
		mil = 2
		birth_date = 281.1.1
		death_date = 341.1.1
		religion = eternals
		culture = ascended
		claim = 100
	}
}