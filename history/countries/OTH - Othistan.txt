government = monarchy
government_rank = 2
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = tyrgarde
capital = 794

300.1.1 = {
	monarch = {
		name = "Wynstan"
		dynasty = "Stormrider"
		adm = 2
		dip = 1
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = tyrgarde
	}
	heir = {
		name = "Zoran"
		dynasty = "Ravenspire"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = tyrgarde
		claim = 100
	}
}