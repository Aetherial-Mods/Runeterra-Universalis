government = monarchy
government_rank = 1
mercantilism = 0
technology_group = noxian
religion = cult_of_the_black_rose
primary_culture = ravenel
capital = 228

300.1.1 = {
	monarch = {
		name = "Taladus"
		dynasty = "Aethal"
		adm = 3
		dip = 6
		mil = 3
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = cult_of_the_black_rose
		culture = ravenel
	}
	heir = {
		name = "Tanker"
		dynasty = "Aethal"
		adm = 5
		dip = 3
		mil = 3
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = cult_of_the_black_rose
		culture = ravenel
		claim = 100
	}
}