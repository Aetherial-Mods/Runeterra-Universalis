government = theocracy
government_rank = 2
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = valerian
capital = 165
elector = yes

300.1.1 = {
	monarch = {
		name = "Akarin"
		dynasty = "Shovuldun"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = valerian
	}
	heir = {
		name = "Candy"
		dynasty = "Shovuldun"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = valerian
		claim = 100
	}
}