government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = nashramaen
capital = 1608

300.1.1 = {
	monarch = {
		name = "Setiu"
		dynasty = "Pheleoas"
		adm = 1
		dip = 4
		mil = 2
		birth_date = 279.1.1
		death_date = 339.1.1
		religion = eternals
		culture = nashramaen
	}
	heir = {
		name = "Dushera"
		dynasty = "Pheleoas"
		adm = 2
		dip = 3
		mil = 5
		birth_date = 294.1.1
		death_date = 354.1.1
		religion = eternals
		culture = nashramaen
		claim = 100
	}
}