government = monarchy
government_rank = 1
mercantilism = 0
technology_group = noxian
religion = lamb_and_wolf
primary_culture = farajan
capital = 429

300.1.1 = {
	monarch = {
		name = "Adrian"
		dynasty = "Blackshear"
		adm = 4
		dip = 1
		mil = 3
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = lamb_and_wolf
		culture = cofauna
	}
	heir = {
		name = "Magnus"
		dynasty = "Blackshear"
		adm = 2
		dip = 5
		mil = 5
		birth_date = 288.1.1
		death_date = 348.1.1
		religion = lamb_and_wolf
		culture = cofauna
		claim = 100
	}
}