government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 94

300.1.1 = {
	monarch = {
		name = "Baladir"
		dynasty = "Barbae"
		adm = 2
		dip = 3
		mil = 4
		birth_date = 276.1.1
		death_date = 336.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Shamara	"
		dynasty = "Barbae"
		adm = 1
		dip = 1
		mil = 2
		birth_date = 291.1.1
		death_date = 351.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}