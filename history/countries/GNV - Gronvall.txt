government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = frostguards
primary_culture = frostguards
capital = 1008

300.1.1 = {
	monarch = {
		name = "Peran"
		adm = 1
		dip = 6
		mil = 3
		birth_date = 272.1.1
		death_date = 332.1.1
		religion = frostguards
		culture = frostguards
	}
}