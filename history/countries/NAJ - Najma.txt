government = tribal
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1572

300.1.1 = {
	monarch = {
		name = "Zakhiro"
		dynasty = "Khashaba"
		adm = 3
		dip = 3
		mil = 5
		birth_date = 264.1.1
		death_date = 324.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Khaldun"
		dynasty = "Khashaba"
		adm = 5
		dip = 3
		mil = 1
		birth_date = 279.1.1
		death_date = 339.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}