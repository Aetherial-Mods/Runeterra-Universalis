government = theocracy
government_rank = 2
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = veilstead
capital = 323
elector = yes

300.1.1 = {
	monarch = {
		name = "Aricen"
		dynasty = "Sturbridge"
		adm = 3
		dip = 2
		mil = 5
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = veilstead
	}
	heir = {
		name = "Thorian"
		dynasty = "Sturbridge"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = veilstead
		claim = 100
	}
}