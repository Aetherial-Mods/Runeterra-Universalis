government = native
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = wanderers
capital = 57

300.1.1 = {
	monarch = {
		name = "Yngvar"
		dynasty = "Bo"
		adm = 2
		dip = 3
		mil = 4
		birth_date = 281.1.1
		death_date = 341.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Tyroe"
		dynasty = "Bo"
		adm = 3
		dip = 6
		mil = 6
		birth_date = 296.1.1
		death_date = 356.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}