government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_veiled_lady
primary_culture = tyrgarde
capital = 696

300.1.1 = {
	monarch = {
		name = "Alaric"
		dynasty = "Song"
		adm = 4
		dip = 3
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_veiled_lady
		culture = tyrgarde
	}
	heir = {
		name = "Zephyr"
		dynasty = "Song"
		adm = 2
		dip = 1
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_veiled_lady
		culture = tyrgarde
		claim = 100
	}
}