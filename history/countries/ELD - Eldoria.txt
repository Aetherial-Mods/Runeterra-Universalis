government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = cassian
capital = 1550

300.1.1 = {
	monarch = {
		name = "Yngvarr"
		dynasty = "Nightshade"
		adm = 6
		dip = 2
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = cassian
	}
	heir = {
		name = "Baelric"
		dynasty = "Nightshade"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = cassian
		claim = 100
	}
}