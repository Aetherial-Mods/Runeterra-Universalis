government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = frostguards
primary_culture = frostguards
capital = 1597

300.1.1 = {
	monarch = {
		name = "Kalkia"
		adm = 3
		dip = 5
		mil = 2
		birth_date = 277.1.1
		death_date = 337.1.1
		religion = frostguards
		culture = frostguards
		female = yes
	}
}