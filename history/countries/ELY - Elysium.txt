government = tribal
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = ebonari
capital = 357

300.1.1 = {
	monarch = {
		name = "Rhydianus"
		dynasty = "Delaney"
		adm = 3
		dip = 6
		mil = 3
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = ebonari
	}
	heir = {
		name = "Tymor"
		dynasty = "Delaney"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = ebonari
		claim = 100
	}
}