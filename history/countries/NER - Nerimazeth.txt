government = monarchy
government_rank = 1
mercantilism = 10
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1191

300.1.1 = {
	monarch = {
		name = "Badur"
		dynasty = "Khashaba"
		adm = 2
		dip = 4
		mil = 3
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = farajan
	}
}