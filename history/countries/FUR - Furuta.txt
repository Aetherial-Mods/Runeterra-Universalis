government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = magma_school
primary_culture = zhyun
capital = 1340

300.1.1 = {
	monarch = {
		name = "Ringo"
		dynasty = "Furutsu"
		adm = 0
		dip = 3
		mil = 2
		birth_date = 270.7.15
		death_date = 330.9.28
		religion = spirit_blossom
		culture = zhyun
	}
	heir = {
		name = "Suika"
		dynasty = "Furutsu"
		adm = 2
		dip = 5
		mil = 4
		birth_date = 285.7.16
		death_date = 345.6.19
		religion = spirit_blossom
		culture = zhyun
		claim = 100
	}
}