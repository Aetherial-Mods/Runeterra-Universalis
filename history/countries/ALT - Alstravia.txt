government = republic
government_rank = 1
mercantilism = 0
technology_group = demacian
religion = the_winged_protector
primary_culture = aelarian
capital = 60

300.1.1 = {
	monarch = {
		name = "Caelan"
		dynasty = "Emberstone"
		adm = 4
		dip = 6
		mil = 1
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = the_winged_protector
		culture = aelarian
	}
	heir = {
		name = "Belthor"
		dynasty = "Pendleton"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 295.1.1
		death_date = 355.1.1
		religion = the_winged_protector
		culture = aelarian
		claim = 100
	}
}