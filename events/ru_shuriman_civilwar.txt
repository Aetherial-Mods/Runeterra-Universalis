namespace = ru_shuriman_civilwar
country_event = {
	id = ru_shuriman_civilwar.1
	title = ru_shuriman_civilwar.1.t
	desc = ru_shuriman_civilwar.1.d
	picture = ru_nerimazeth_eventPicture

	fire_only_once = yes
	
	trigger = {
		tag = MNS
		is_subject = no
		exists = SHU
		NOT = { alliance_with = SHU }
		NOT = { war_with = SHU }
		NOT = { is_year = 305 }
		NOT = { truce_with = SHU }
		SHU = {
			is_subject = no
		}
		is_year = 301
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "ru_shuriman_civilwar.1.a"
		tooltip = {
			958 = {
				cede_province = SHU
			}
			363 = {
				cede_province = SHU
			}
			381 = {
				cede_province = SHU
			}
			add_truce_with = SHU
		}
		SHU = { country_event = { id = ru_shuriman_civilwar.3 } }
		add_prestige = -10
		add_stability = -1
		
		ai_chance = {
			factor = 10
		}
	}
	option = {
		name = "ru_shuriman_civilwar.1.b"
		SHU = { country_event = { id = ru_shuriman_civilwar.2 } }
		
		ai_chance = {
			factor = 90
		}
	}
}

country_event = {
	id = ru_shuriman_civilwar.2
	title = ru_shuriman_civilwar.2.t
	desc = ru_shuriman_civilwar.2.d
	picture = ru_nerimazeth_eventPicture
	
	is_triggered_only = yes

	major = yes
	
	option = {
		name = "ru_shuriman_civilwar.2.a"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 0
				is_at_war = yes
			}
			modifier = {
				factor = 0
				has_any_disaster = yes
			}
		}
		MNS = {
			declare_war_with_cb = {
				who = SHU
				casus_belli = cb_shuriman_civilwar
			}
		}
		958 = {
			change_controller = SHU
		}
		363 = {
			change_controller = SHU
		}
		381 = {
			change_controller = SHU
		}
	}
	option = {
		name = "ru_shuriman_civilwar.2.b"
		ai_chance = {
			factor = 1
		}
		add_prestige = -10
		FROM = {
			add_truce_with = ROOT
		}
		add_truce_with = FROM
	}
}

country_event = {
	id = ru_shuriman_civilwar.3
	title = ru_shuriman_civilwar.3.t
	desc = ru_shuriman_civilwar.3.d
	picture = ru_nerimazeth_eventPicture
	
	is_triggered_only = yes
	
	major = yes

	option = {
		name = "ru_shuriman_civilwar.3.a"
		add_prestige = 10
		hidden_effect = {
			FROM = {
				add_truce_with = ROOT
			}
		}
		add_truce_with = FROM
		958 = {
			cede_province = SHU
		}
		363 = {
			cede_province = SHU
		}
		381 = {
			cede_province = SHU
		}
	}
}