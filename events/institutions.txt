#######################################
# Harmony 
#######################################

namespace = institution_events


#######################################
# Scholasticism
#######################################
country_event = {
	id = institution_events.2
	title = institution_events.2.t
	desc = institution_events.2.d
	
	picture = PAINTER_ARTIST_eventPicture
	
	is_triggered_only = yes

	goto = institution_origin
	
	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = {
		hidden_effect = {
			from = { save_event_target_as = institution_origin }
		}
	}

	option = {
		name = institution_events.2.a
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		add_prestige = 20
		custom_tooltip = institution_events.2.a.t
		FROM = {
			add_permanent_province_modifier = {
				name = "birthplace_of_the_renaissance"
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					knows_country = ROOT
				}
				country_event = { id = institution_events.21 }
			}
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					NOT = { knows_country = ROOT }
				}
				country_event = { id = institution_events.22 }
			}
		}
	}
}
country_event = {
	id = institution_events.21
	title = institution_events.2.t
	desc = institution_events.21.d
	picture = PAINTER_ARTIST_eventPicture

	goto = institution_origin
	
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.2.a
		custom_tooltip = institution_events.2.a.t
	}
}
country_event = {
	id = institution_events.22
	title = institution_events.2.t
	desc = institution_events.22.d
	picture = PAINTER_ARTIST_eventPicture

	is_triggered_only = yes

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.2.a
		custom_tooltip = institution_events.2.a.t
	}
}


#######################################
# Expansionism
#######################################
country_event = {
	id = institution_events.3
	title = institution_events.3.t
	desc = institution_events.3.d
	
	picture = PAINTER_ARTIST_eventPicture
	
	is_triggered_only = yes

	goto = institution_origin
	
	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = {
		hidden_effect = {
			from = { save_event_target_as = institution_origin }
		}
	}
	
	option = {
		name = institution_events.3.a
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		add_prestige = 20
		custom_tooltip = institution_events.3.a.t
		FROM = {
			add_permanent_province_modifier = {
				name = "birthplace_of_the_renaissance"
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					knows_country = ROOT
				}
				country_event = { id = institution_events.31 }
			}
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					NOT = { knows_country = ROOT }
				}
				country_event = { id = institution_events.32 }
			}
		}
	}
}
country_event = {
	id = institution_events.31
	title = institution_events.3.t
	desc = institution_events.31.d
	picture = PAINTER_ARTIST_eventPicture

	goto = institution_origin
	
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.3.a
		custom_tooltip = institution_events.3.a.t
	}
}
country_event = {
	id = institution_events.32
	title = institution_events.3.t
	desc = institution_events.32.d
	picture = PAINTER_ARTIST_eventPicture

	is_triggered_only = yes

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.3.a
		custom_tooltip = institution_events.3.a.t
	}
}


#######################################
# Feudalism
#######################################
country_event = {
	id = institution_events.4
	title = institution_events.4.t
	desc = institution_events.4.d
	
	picture = PAINTER_ARTIST_eventPicture
	
	is_triggered_only = yes

	goto = institution_origin
	
	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = {
		hidden_effect = {
			from = { save_event_target_as = institution_origin }
		}
	}
	
	option = {
		name = institution_events.4.a
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		add_prestige = 20
		custom_tooltip = institution_events.4.a.t
		FROM = {
			add_permanent_province_modifier = {
				name = "birthplace_of_the_renaissance"
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					knows_country = ROOT
				}
				country_event = { id = institution_events.41 }
			}
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					NOT = { knows_country = ROOT }
				}
				country_event = { id = institution_events.42 }
			}
		}
	}
}
country_event = {
	id = institution_events.41
	title = institution_events.4.t
	desc = institution_events.41.d
	picture = PAINTER_ARTIST_eventPicture

	goto = institution_origin
	
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.4.a
		custom_tooltip = institution_events.4.a.t
	}
}
country_event = {
	id = institution_events.42
	title = institution_events.4.t
	desc = institution_events.42.d
	picture = PAINTER_ARTIST_eventPicture

	is_triggered_only = yes

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.4.a
		custom_tooltip = institution_events.4.a.t
	}
}


#######################################
# Devotion
#######################################
country_event = {
	id = institution_events.5
	title = institution_events.5.t
	desc = institution_events.5.d
	
	picture = PAINTER_ARTIST_eventPicture
	
	is_triggered_only = yes

	goto = institution_origin
	
	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = {
		hidden_effect = {
			from = { save_event_target_as = institution_origin }
		}
	}
	
	option = {
		name = institution_events.5.a
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		add_prestige = 20
		custom_tooltip = institution_events.5.a.t
		FROM = {
			add_permanent_province_modifier = {
				name = "birthplace_of_the_renaissance"
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					knows_country = ROOT
				}
				country_event = { id = institution_events.51 }
			}
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					NOT = { knows_country = ROOT }
				}
				country_event = { id = institution_events.52 }
			}
		}
	}
}
country_event = {
	id = institution_events.51
	title = institution_events.5.t
	desc = institution_events.51.d
	picture = PAINTER_ARTIST_eventPicture

	goto = institution_origin
	
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.5.a
		custom_tooltip = institution_events.5.a.t
	}
}
country_event = {
	id = institution_events.52
	title = institution_events.5.t
	desc = institution_events.52.d
	picture = PAINTER_ARTIST_eventPicture

	is_triggered_only = yes

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.5.a
		custom_tooltip = institution_events.5.a.t
	}
}


#######################################
# Resurgence of Civilization
#######################################
country_event = {
	id = institution_events.6
	title = institution_events.6.t
	desc = institution_events.6.d
	
	picture = PAINTER_ARTIST_eventPicture
	
	is_triggered_only = yes

	goto = institution_origin
	
	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = {
		hidden_effect = {
			from = { save_event_target_as = institution_origin }
		}
	}
	
	option = {
		name = institution_events.6.a
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		add_prestige = 20
		custom_tooltip = institution_events.6.a.t
		FROM = {
			add_permanent_province_modifier = {
				name = "birthplace_of_the_renaissance"
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					knows_country = ROOT
				}
				country_event = { id = institution_events.61 }
			}
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
					NOT = { knows_country = ROOT }
				}
				country_event = { id = institution_events.62 }
			}
		}
	}
}
country_event = {
	id = institution_events.61
	title = institution_events.6.t
	desc = institution_events.61.d
	picture = PAINTER_ARTIST_eventPicture

	goto = institution_origin
	
	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.6.a
		custom_tooltip = institution_events.6.a.t
	}
}
country_event = {
	id = institution_events.62
	title = institution_events.6.t
	desc = institution_events.62.d
	picture = PAINTER_ARTIST_eventPicture

	is_triggered_only = yes

	trigger = {
		always = no
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = institution_events.6.a
		custom_tooltip = institution_events.6.a.t
	}
}