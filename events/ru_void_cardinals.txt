namespace = ru_void_cardinals
country_event = {
	id = ru_void_cardinals.1
	title = ru_void_cardinals.1.t
	desc = ru_void_cardinals.1.d
	picture = ru_void_cardinal_eventPicture

	trigger = {
		technology_group = void
		religion = the_watchers
		culture_group = void
		NOT = { num_of_cardinals = 1 }
	}
	
	option = {
		name = ru_void_cardinals.1.a
			
		capital_scope = {
			add_cardinal = yes
		}
		
		ai_chance = {
			factor = 1
		}
	}
}